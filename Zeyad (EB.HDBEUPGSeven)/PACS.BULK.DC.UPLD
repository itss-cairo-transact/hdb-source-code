*-----------------------------------------------------------------------------------------------------------------*
* <Rating>-52</Rating>
*-----------------------------------------------------------------------------------------------------------------*

    SUBROUTINE PACS.BULK.DC.UPLD(TEMP.ID)

*-----------------------------------------------------------------------------------------------------------------*
*
* AUTHOR            :       P.SATSH KUMAR
* DATE              :       19/08/2016
* PURPOSE           :       Multi-Threaded routine to post singled-sided DC as a correction.
* INPUT             :       BATCH > DATA > BULK.DC.LIST
* INPUT Format      :       COMPANYCODE#ACCOUNT.NUMBER#SIGN#AMOUNT.LCY#TRANSACTION.CODE#NARRATIVE#PL.CATEGORY
*                           #ACCOUNT.OFFICER#PRODUCT.CATEGORY#VALUE.DATE#CURRENCY#AMOUNT.FCY#EXCHANGE.RATE
* OUTPUT DIRECTORY  :       COMO,OFS.REQUEST.DETAIL > ID's prefixed with 'DCCOR' value
* Developed for     :       GL corrections
*
*-----------------------------------------------------------------------------------------------------------------*

    $INSERT I_EQUATE
    $INSERT I_COMMON
    $INSERT I_F.DATA.CAPTURE
    $INSERT I_F.DC.BATCH.CONTROL
    $INSERT I_PACS.BULK.DC.UPLD.COMMON
    $INSERT I_F.ACCOUNT.PARAMETER

*-----------------------------------------------------------------------------------------------------------------*

    GOSUB INIT
    GOSUB INPUT.DC
    GOSUB PRINT.TXN.STATUS

    IF TXN.STATUS EQ '1' THEN
        GOSUB AMEND.DCBC
        GOSUB AUTH.DC
        GOSUB PRINT.TXN.STATUS
    END

    RETURN

*--------------------------------------------------------------------------------------------------------------*

INIT:
    IF R.ACCOUNT.PARAMETER<AC.PAR.DC.BALANCED> EQ 'LOCK' THEN
        OLD.DC.BALANCED=R.ACCOUNT.PARAMETER<AC.PAR.DC.BALANCED>
        R.ACCOUNT.PARAMETER<AC.PAR.DC.BALANCED> = ''
    END

    FAILED.REC='';FAILED.REC.KEY='';PROCESSED.REC=''

    RETURN

*--------------------------------------------------------------------------------------------------------------*

INPUT.DC:

    DC.CORR='';R.DCBC=''
    COMP.CODE=FIELD(TEMP.ID,'#',1)

    BEGIN CASE
    CASE COMP.CODE AND COMP.CODE NE ID.COMPANY
        CALL LOAD.COMPANY(COMP.CODE)

    CASE NOT(COMP.CODE)
        PRINT "COMPANY.CODE MISSING - IT'S MANDATORY INPUT"
        RETURN

    END CASE

    DC.CORR<DC.DC.ACCOUNT.NUMBER>=FIELD(TEMP.ID,'#',2)
    DC.CORR<DC.DC.SIGN>=FIELD(TEMP.ID,'#',3)
    DC.CORR<DC.DC.AMOUNT.LCY>=FIELD(TEMP.ID,'#',4)
    DC.CORR<DC.DC.TRANSACTION.CODE>=FIELD(TEMP.ID,'#',5)
    DC.CORR<DC.DC.NARRATIVE,1>=FIELD(TEMP.ID,'#',6)
    DC.CORR<DC.DC.PL.CATEGORY>=FIELD(TEMP.ID,'#',7)
    DC.CORR<DC.DC.ACCOUNT.OFFICER>=FIELD(TEMP.ID,'#',8)
    DC.CORR<DC.DC.PRODUCT.CATEGORY>=FIELD(TEMP.ID,'#',9)
    DC.CORR<DC.DC.VALUE.DATE>=FIELD(TEMP.ID,'#',10)
    DC.CORR<DC.DC.CURRENCY>=FIELD(TEMP.ID,'#',11)
    DC.CORR<DC.DC.AMOUNT.FCY>=FIELD(TEMP.ID,'#',12)
    DC.CORR<DC.DC.EXCHANGE.RATE>=FIELD(TEMP.ID,'#',13)

    ACC.ID=FIELD(TEMP.ID,'#',2)
    ACCT.OFF=FIELD(TEMP.ID,'#',8)
    PL.CATEG=FIELD(TEMP.ID,'#',7)
    PRD.CDE=FIELD(TEMP.ID,'#',9)

    DC.ID='NEW';options="DC.OFS.PACS"

    FUNCT='I';OFS.RECORD='';OFSVERSION="DATA.CAPTURE,PACS.CORRECTION"

    CALL OFS.BUILD.RECORD(APP.NAME, FUNCT, "PROCESS", OFSVERSION, "", NO.OF.AUTH, DC.ID, DC.CORR, OFS.RECORD)
    CALL OFS.CALL.BULK.MANAGER(options, OFS.RECORD, OFS.RESP, "")
    FUNCT.DESC='input'

    RETURN

*--------------------------------------------------------------------------------------------------------------*
AMEND.DCBC:

    DC.ID.LEN=LEN(DC.ID)-3;DCBC.ER=""
    R.DCBC='';DCBC.ID=DC.ID[1,DC.ID.LEN]

    CALL F.READU(FN.DCBC,DCBC.ID,R.DCBC,F.DCBC,DCBC.ER,'')

    IF R.DCBC THEN
        LCY.DB.AMT=R.DCBC<DC.BAT.LCY.AMOUNT.DEBIT>
        LCY.CR.AMT=R.DCBC<DC.BAT.LCY.AMOUNT.CREDIT>
        FCY.DB.AMT=R.DCBC<DC.BAT.FCY.AMOUNT.DEBIT>
        FCY.CR.AMT=R.DCBC<DC.BAT.FCY.AMOUNT.CREDIT>

        IF (FCY.DB.AMT NE '') AND (FCY.CR.AMT EQ '') THEN
            R.DCBC<DC.BAT.FCY.AMOUNT.CREDIT> = FCY.DB.AMT
        END

        IF (FCY.CR.AMT NE '') AND (FCY.DB.AMT EQ '') THEN
            R.DCBC<DC.BAT.FCY.AMOUNT.DEBIT> = FCY.CR.AMT
        END

        IF (LCY.DB.AMT NE '') AND (LCY.CR.AMT EQ '') THEN
            R.DCBC<DC.BAT.LCY.AMOUNT.CREDIT> = LCY.DB.AMT
        END

        IF (LCY.CR.AMT NE '') AND (LCY.DB.AMT EQ '') THEN
            R.DCBC<DC.BAT.LCY.AMOUNT.DEBIT> = LCY.CR.AMT
        END

        CALL F.WRITE(FN.DCBC,DCBC.ID, R.DCBC)     ;*update balanced DC.BATCH.CONTROL
    END ELSE
        PRINT 'Unnable to read DC.BATCH.CONTROL - ':DCBC.ID
    END

    RETURN

*--------------------------------------------------------------------------------------------------------------*
AUTH.DC:

    FUNCT='A';OFS.RECORD='';OFS.RESP='';DC.CORR=''

    CALL OFS.BUILD.RECORD(APP.NAME, FUNCT, "PROCESS", OFSVERSION, "", NO.OF.AUTH, DC.ID, DC.CORR, OFS.RECORD)
    CALL OFS.CALL.BULK.MANAGER(options, OFS.RECORD, OFS.RESP, "")

    OUT.REF='';TXN.STATUS=''
    OUT.REF=FIELD(OFS.RESP,',',1)
    DC.ID=FIELD(OUT.REF,'/',1)
    TXN.STATUS=FIELD(OUT.REF,'/',3)

    IF OLD.DC.BALANCED THEN
        R.ACCOUNT.PARAMETER<AC.PAR.DC.BALANCED> = OLD.DC.BALANCED     ;*Old values assaigned back.
    END

    FUNCT.DESC="authorisation"
    RETURN

*--------------------------------------------------------------------------------------------------------------*
PRINT.TXN.STATUS:
*---------------*
    IF OFS.RESP THEN
        OUT.REF=''
        OUT.REF=FIELD(OFS.RESP,',',1)
        DC.ID=FIELD(OUT.REF,'/',1)
        TXN.STATUS=FIELD(OUT.REF,'/',3)

        IF TXN.STATUS EQ "-1" THEN
            PRINT 'Transaction ':FUNCT.DESC:' failed - ':DC.ID
        END ELSE
            PRINT 'Transaction ':FUNCT.DESC' sucessfull - ':DC.ID
        END
    END

    RETURN
*--------------------------------------------------------------------------------------------------------------*
END
*--------------------------------------------------------------------------------------------------------------*






