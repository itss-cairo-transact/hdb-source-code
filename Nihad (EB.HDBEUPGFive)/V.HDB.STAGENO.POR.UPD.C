    SUBROUTINE V.HDB.STAGENO.POR.UPD.C

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.HDB.PORTAL.PAY.CUST
	
	GOSUB PROCESS
     GOSUB PROG.END
	
	RETURN
	
*************************************************************************
PROCESS:
*******
DEBUG
      
    Y.CD.ID = ""
    RET.CODE = ""
    R.CD = ""
    FN.HDB = "F.HDB.PORTAL.PAY.CUST"
    F.HDB = ""
    CALL OPF(FN.HDB,F.HDB)

    VAL00 = ID.NEW[1,3]
    IF VAL00 <> "JAN" THEN
       E = "AA-AC.HDB.LAND.ISKAN.NEW"
         CALL STORE.END.ERROR
         GOSUB PROG.END
    END
    Y.CD.ID = ID.NEW
    CALL F.READ(FN.HDB,Y.CD.ID,R.CD,F.HDB,RET.CODE) 
     IF RET.CODE  <> "" THEN
         E = "AA-AC.HDB.LAND.ISKAN.NEW"
         CALL STORE.END.ERROR
         GOSUB PROG.END
     END
     IF RET.CODE  EQ "" AND R.CD<PAYCUST.INFO.FLG.P1> = 0 THEN
         E = "AA-AC.HDB.LAND.ISKAN.R4"
         CALL STORE.END.ERROR
         GOSUB PROG.END
     END
    
    RETURN
	
*************************************************************************
PROG.END:


RETURN
*********

END
