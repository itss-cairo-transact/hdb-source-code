    SUBROUTINE V.HDB.PORTAL.BRANCH.TST

    $INCLUDE I_COMMON
    $INCLUDE I_EQUATE
    $INSERT I_F.MBSC.ACH.BRANCH

*************************************************************************
PROCESS:
********
DEBUG
    FN.HDB = "F.MBSC.ACH.BRANCH"
    F.HDB = ""
    R.CD = ""
    RET.CODE = ""
    
    CALL OPF(FN.HDB,F.HDB)
    Y.CD.ID = COMI
    CALL F.READ(FN.HDB,Y.CD.ID,R.CD,F.HDB,RET.CODE) 
    IF RET.CODE <> "" THEN
         ETEXT = "AA-AC.HDB.PORTAL.R1"
         CALL STORE.END.ERROR
         GOSUB PROG.END
    END   

    RETURN

*************************************************************************
PROG.END:
*********
END
RETURN
