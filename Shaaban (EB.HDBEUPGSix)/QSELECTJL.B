      PROGRAM QSELECTJL

      IF NOT(SENTENCE(1)) OR NOT(NUM(SENTENCE(1))) THEN
         CRT "Usage: QSELECTJL job_list_num"
         EXIT(1)
      END

      FN.JOB.LIST = "F.JOB.LIST.":SENTENCE(1) ; F.JOB.LIST = ""
      OPEN FN.JOB.LIST TO F.JOB.LIST ELSE
         CRT "Cannot open job list file ":SQUOTE(FN.JOB.LIST)
         EXIT(1)
      END

      CRT OCONV(TIME(), "MTS"):" - Start scanning"
      PENDING.JOB.LIST = 0
      PENDING.FIELDS = 0
      BULK.NO = 1
      SELECT F.JOB.LIST
      LOOP READNEXT JOB.LIST.ID ELSE JOB.LIST.ID = "" WHILE JOB.LIST.ID <> ""
         R.JOB.LIST = ""
         READ R.JOB.LIST FROM F.JOB.LIST, JOB.LIST.ID THEN
            PENDING.JOB.LIST += 1
            IF PENDING.JOB.LIST = 1 THEN BULK.NO = DCOUNT(R.JOB.LIST<1>, @VM)
            PENDING.FIELDS += DCOUNT(R.JOB.LIST, @FM) * BULK.NO
         END
      REPEAT

      CLOSE F.JOB.LIST

      CRT OCONV(TIME(), "MTS"):" - Pending Job Lists = ":FMT(PENDING.JOB.LIST, ","):" - Pending Contracts = ":FMT(PENDING.FIELDS, ",")

      EXIT(0)

