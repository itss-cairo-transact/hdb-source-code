*-----------------------------------------------------------------------------
* <Rating>1090</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE TREASURY.CORRECTION

*-----------------------------------------------------------------------------*
* This is a mainline routine to create Single-side DC reading a csv file      *
* The format of the CSV file should be                                        *
* Account Number,D/C,Amount.FCY,Amount.LCY,CCY,Narrative,Company Code         *
* The routine uses OGM, and OFS.SOURCE is hardcoded to TESTOFS :-)            *
* If required OFS source and username/pass to be changed accordingly          *
*-----------------------------------------------------------------------------*

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_GTS.COMMON
    $INSERT I_F.COMPANY
    $INSERT I_F.USER
    $INSERT I_F.EB.POSITION.PARAMETER
    $INSERT I_F.POSITION
    $INSERT I_F.DATES
    $INSERT I_F.CONSOLIDATE.COND
    $INSERT I_F.EB.CONTRACT.BALANCES
    $INSERT I_F.CONSOL.ENT.TODAY
    $INSERT I_F.CONSOL.UPDATE.WORK

    T.STAMP = OCONV(TIME(),"MTS")
*     EXECUTE 'COMO ON TREASURY.CORRECTION_':TODAY:'_':T.STAMP
    EXECUTE 'COMO ON TREASURY.CORRECTION'
    GOSUB OPEN.FILES
    GOSUB PROCESS
    EXECUTE 'COMO OFF'
!
    RETURN
!

*-----------------------------------------------------------------------------
OPEN.FILES:
*-----------------------------------------------------------------------------

    FN.DC.BATCH.CONTROL = 'F.DC.BATCH.CONTROL'
    F.DC.BATCH.CONTROL = ''
    CALL OPF(FN.DC.BATCH.CONTROL,F.DC.BATCH.CONTROL)

    FN.EB.POSITION.PARAMETER = 'F.EB.POSITION.PARAMETER'
    F.EB.POSITION.PARAMETER = ''; CALL OPF(FN.EB.POSITION.PARAMETER,F.EB.POSITION.PARAMETER)

    FN.POSITION = 'F.POSITION'
    F.POSITION = ''; CALL OPF(FN.POSITION, F.POSITION)

    FN.RE.CONSOL.CONTRACT = 'F.RE.CONSOL.CONTRACT'
    F.RE.CONSOL.CONTRACT = ''; CALL OPF(FN.RE.CONSOL.CONTRACT, F.RE.CONSOL.CONTRACT)

    FN.EB.CONTRACT.BALANCES = 'F.EB.CONTRACT.BALANCES'
    F.EB.CONTRACT.BALANCES = ''; CALL OPF(FN.EB.CONTRACT.BALANCES, F.EB.CONTRACT.BALANCES)

    FN.CONSOLIDATE.COND = 'F.CONSOLIDATE.COND'
    F.CONSOLIDATE.COND = ''; CALL OPF(FN.CONSOLIDATE.COND,F.CONSOLIDATE.COND)

    FN.CONSOL.ENT.TODAY = 'F.CONSOL.ENT.TODAY'
    F.CONSOL.ENT.TODAY = ''; CALL OPF(FN.CONSOL.ENT.TODAY, F.CONSOL.ENT.TODAY)

    FN.EOD.CONSOL.UPDATE.DETAIL = 'F.EOD.CONSOL.UPDATE.DETAIL'
    F.EOD.CONSOL.UPDATE.DETAIL = ''; CALL OPF(FN.EOD.CONSOL.UPDATE.DETAIL,F.EOD.CONSOL.UPDATE.DETAIL)

    FN.CONSOL.UPDATE.WORK = 'F.CONSOL.UPDATE.WORK'
    F.CONSOL.UPDATE.WORK = ''; CALL OPF(FN.CONSOL.UPDATE.WORK,F.CONSOL.UPDATE.WORK)

    FN.COMPANY = 'F.COMPANY'
    F.COMPANY = ''
    CALL OPF(FN.COMPANY,F.COMPANY)

    Y.POS.DATE = R.DATES(EB.DAT.LAST.WORKING.DAY)

    OPEN "&SAVEDLISTS&" TO F.CORR ELSE
        CALL OCOMO("UNABLE TO OPEN PACS.BP")
    END

    COND.ID = 'ASSET&LIAB';R.COND = ''
    READ R.COND FROM F.CONSOLIDATE.COND, COND.ID THEN
        POS.ENTRY = R.COND<RE.CON.POSITION.ENTRY>
    END

    OFS.HEADER = "DATA.CAPTURE,/I/PROCESS,//"

    RETURN

*-----------------------------------------------------------------------------
PROCESS:
*-----------------------------------------------------------------------------

    CALL OCOMO("INPUT THE FILE NAME IN PACS.BP")
    INPUT CSV.FILE
!

    IF CSV.FILE THEN
        READ R.CSV FROM F.CORR,CSV.FILE THEN
            LOOP
                REMOVE CSV.LINE FROM R.CSV SETTING CSV.POS
            WHILE CSV.LINE:CSV.POS
                 
                AC.NO = '' ; DC.MARKER = '' ; AMT.FCY = '' ; AMT.LCY = '' ; CCY = '' ; NARRATIVE = '' ; CO.CODE = ''
                AC.NO     = FIELD(CSV.LINE,',',1)
                DC.MARKER = FIELD(CSV.LINE,',',2)
                AMT.FCY   = FIELD(CSV.LINE,',',3)
                AMT.LCY   = FIELD(CSV.LINE,',',4)
                CCY       = FIELD(CSV.LINE,',',5)
*                IF CCY EQ LCCY THEN
*                    AMT.FCY   = FIELD(CSV.LINE,',',3)
*                    AMT.LCY   = FIELD(CSV.LINE,',',4)
*                END ELSE
*                    AMT.FCY   = FIELD(CSV.LINE,',',3)
*                    AMT.LCY   = FIELD(CSV.LINE,',',4)
*                END
                NARRATIVE = FIELD(CSV.LINE,',',6)
                CO.CODE   = FIELD(CSV.LINE,',',7)
                EXCH.RATE = FIELD(CSV.LINE,',',8)
                PL.CATEG = FIELD(AC.NO,'PL',2)
                MISMATCH.KEY = FIELD(CSV.LINE,',',10)
                NO.DOT = COUNT(MISMATCH.KEY,".")
                ASST.TYPE = FIELD(MISMATCH.KEY,".",NO.DOT+1)
                CET.KEY = MISMATCH.KEY[1,(LEN(MISMATCH.KEY) - LEN(ASST.TYPE)-1)]
                POSN.FLAG = FIELD(CSV.LINE,',',11)
                ECB.FLAG = FIELD(CSV.LINE,',',12)
                RCC.FLAG = FIELD(CSV.LINE,',',13)
                TXN.REF = FIELD(CSV.LINE,',',9)
                PRD = FIELD(MISMATCH.KEY,".",1)
                MKT = FIELD(MISMATCH.KEY,".",2)
                PROD.CATEG = FIELD(CSV.LINE,",",14)

                GOSUB CALL.EXCHANGE

                IF AC.NO THEN
                    GOSUB FORM.OFS.STR
                END

                IF CET.KEY THEN
                    GOSUB CET.RAISE
                    GOSUB CHECK.ASST.TYPE         ;*Check if asset type is contingent
                END

                IF POSN.FLAG EQ 'Y' THEN
                    GOSUB CREATE.POS.ACCT
                    GOSUB CREATE.POSITION
                END

                IF ECB.FLAG EQ 'Y' THEN
                    GOSUB UPDATE.ECB
                END

                IF RCC.FLAG EQ 'Y' THEN
                    GOSUB UPDATE.RCC
                END

            REPEAT
        END
    END

    RETURN

*-----------------------------------------------------------------------------
CHECK.ASST.TYPE:
*-----------------------------------------------------------------------------

    NON.CONTIG.LIST = ''; NON.CONTINGENT = ''; TEMP.ASST.TYPE=''
    CALL RE.TYPES("ALL.N",NON.CONTIG.LIST)
    SELF.BAL = R.COND<RE.CON.CONT.SELF.BAL>
    CALL AC.CHECK.ASSET.TYPE(ASST.TYPE,NON.CONTIG.LIST,NON.CONTINGENT)
    IF NOT(NON.CONTINGENT) AND SELF.BAL EQ 'Y' THEN
        GOSUB UPDATE.BL.ENTRY
    END
    RETURN

*-----------------------------------------------------------------------------
FORM.OFS.STR:
*-----------------------------------------------------------------------------

    CALL OCOMO('PROCESSING CSV LINE: ':CSV.LINE)

    IF AC.NO[1,2] EQ 'PL' THEN
        GOSUB FORM.PL.OFS
    END ELSE
        OFS.STR  = OFS.HEADER:CO.CODE:",,"
        OFS.STR := "ACCOUNT.NUMBER:1:1=":AC.NO:","
        OFS.STR :="SIGN:1:1=":DC.MARKER:","
        IF CCY NE LCCY THEN
            OFS.STR :="CURRENCY:1:1=":CCY
            OFS.STR :=",EXCHANGE.RATE:1:1=":EXCH.RATE:","
            AMT.FCY *= 1
            IF AMT.FCY NE 0 THEN
                OFS.STR := "AMOUNT.FCY:1:1=":AMT.FCY:","
            END
        END
        IF CCY EQ LCCY THEN
            AMT.LCY *= 1
            IF AMT.LCY NE 0 THEN
                OFS.STR :="AMOUNT.LCY:1:1=":AMT.LCY:","
            END
        END
        IF DC.MARKER EQ 'C' THEN
            OFS.STR :="TRANSACTION.CODE:1:1=51,"
        END ELSE
            OFS.STR :="TRANSACTION.CODE:1:1=1,"
        END
        OFS.STR := "NARRATIVE:1:1=":NARRATIVE
        OFS.STR := ",OUR.REFERENCE:1:1=":TXN.REF[1,12]
    END
    CALL OCOMO('DC OFS MSG: ':OFS.STR)
!
    CALL OFS.GLOBUS.MANAGER("TESTOFS",OFS.STR)
!
    CALL OCOMO('DC OFS RESPONSE: ':OFS.STR)

    GOSUB BALANCE.DC.ENTRY

    RETURN

*-----------------------------------------------------------------------------
FORM.PL.OFS:
*-----------------------------------------------------------------------------

    DAO.ID = ''
    CUST.ID = ''

    DAO.ID = R.USER<EB.USE.DEPARTMENT.CODE>

    OFS.STR  = OFS.HEADER:CO.CODE:",,"
    OFS.STR := "PL.CATEGORY:1:1=":PL.CATEG:","
    OFS.STR :="SIGN:1:1=":DC.MARKER:
    IF CCY NE LCCY THEN
        OFS.STR :=",CURRENCY:1:1=":CCY
        OFS.STR :=",EXCHANGE.RATE:1:1=":EXCH.RATE
        AMT.FCY *= 1
        IF AMT.FCY NE 0 THEN
            OFS.STR := ",AMOUNT.FCY:1:1=":AMT.FCY:","
        END
    END
    IF CCY EQ LCCY THEN
        AMT.LCY *= 1
        IF AMT.LCY NE 0 THEN
            OFS.STR :=",AMOUNT.LCY:1:1=":AMT.LCY:","
        END
    END
    IF DC.MARKER EQ 'C' THEN
        OFS.STR :="TRANSACTION.CODE:1:1=51,"
    END ELSE
        OFS.STR :="TRANSACTION.CODE:1:1=1,"
    END
    OFS.STR := "NARRATIVE:1:1=":NARRATIVE:","
    IF CUST.ID NE '' THEN
        OFS.STR := "CUSTOMER.ID:1:1=":CUST.ID:","
    END ELSE
        OFS.STR := "ACCOUNT.OFFICER:1:1=":DAO.ID:","
    END
    OFS.STR := "OUR.REFERENCE:1:1=":TXN.REF[1,12]
    OFS.STR := ",PRODUCT.CATEGORY:1:1=":PROD.CATEG

    RETURN

*-----------------------------------------------------------------------------
BALANCE.DC.ENTRY:
*-----------------------------------------------------------------------------

    OUTPUT.STR = ''
    OUTPUT.STR = OFS.STR
    IF OUTPUT.STR THEN
        OUTPUT.FLAG = FIELD(FIELD(OUTPUT.STR,"/",3),',',1)
        IF OUTPUT.FLAG EQ '1' THEN
            DC.BATCH.CONTROL.ID = ''; DC.ID = ''
            DC.ID = FIELD(OUTPUT.STR,"/",1)
            DC.BATCH.CONTROL.ID = FIELD(OUTPUT.STR,"/",1)[1,14]
            R.DC.BATCH.CONTROL = ''
            READ R.DC.BATCH.CONTROL FROM F.DC.BATCH.CONTROL,DC.BATCH.CONTROL.ID THEN
                IF R.DC.BATCH.CONTROL<5> NE '' AND R.DC.BATCH.CONTROL<6> EQ '' THEN R.DC.BATCH.CONTROL<6> = R.DC.BATCH.CONTROL<5>
                IF R.DC.BATCH.CONTROL<6> NE '' AND R.DC.BATCH.CONTROL<5> EQ '' THEN R.DC.BATCH.CONTROL<5> = R.DC.BATCH.CONTROL<6>
                IF R.DC.BATCH.CONTROL<3> NE '' AND R.DC.BATCH.CONTROL<4> EQ '' THEN R.DC.BATCH.CONTROL<4> = R.DC.BATCH.CONTROL<3>
                IF R.DC.BATCH.CONTROL<4> NE '' AND R.DC.BATCH.CONTROL<3> EQ '' THEN R.DC.BATCH.CONTROL<3> = R.DC.BATCH.CONTROL<4>
                WRITE R.DC.BATCH.CONTROL TO F.DC.BATCH.CONTROL,DC.BATCH.CONTROL.ID ON ERROR
                    CALL OCOMO("PROBLEM WITH WRITE FOR ":DC.BATCH.CONTROL.ID)
                END
            END
        END
        GOSUB AUTH.DC.ENTRY
        OFS.STR = ''
    END
    RETURN

*-----------------------------------------------------------------------------
AUTH.DC.ENTRY:
*-----------------------------------------------------------------------------

    AUTH.OFS.MSG = "DATA.CAPTURE,/A/PROCESS,//":CO.CODE:",":DC.ID
    CALL OCOMO('DC OFS AUTH MSG:':AUTH.OFS.MSG)
    CALL OFS.GLOBUS.MANAGER("TESTOFS",AUTH.OFS.MSG)
    CALL OCOMO('DC OFS AUTH RESPONSE:':AUTH.OFS.MSG)

    RETURN

*-----------------------------------------------------------------------------
CREATE.POS.ACCT:
*-----------------------------------------------------------------------------


    POS.ACCT.OFS = '' ; Y.AL.CATEG = '' ; Y.FCY.ACCT = '' ; Y.LCY.ACCT = '' ; DEBIT.POS.ACCT = '' ; CREDIT.POS.ACCT = '' ; R.COMP = ''
    IF CCY EQ LCCY THEN
        CALL OCOMO('POSITION NOT REQUIRED FOR LOCAL CURRENCY')
        RETURN
    END

    IF POS.ENTRY NE 'ACCOUNT' THEN
        RETURN
    END
    READ R.COMP FROM F.COMPANY,CO.CODE THEN

        R.EB.POSITION.PARAMETER = ''
        READ R.EB.POSITION.PARAMETER FROM F.EB.POSITION.PARAMETER, ID.COMPANY THEN
*        LOCATE '1TR' IN R.EB.POSITION.PARAMETER<EB.POS.CCYMKT.POSTYPE,1> SETTING POS THEN
            Y.AL.CATEG = R.EB.POSITION.PARAMETER<EB.POS.AL.CATEG,1>
            CALL OCOMO("Y.AL.CATEG :":Y.AL.CATEG)
            Y.FCY.ACCT = CCY:LCCY:Y.AL.CATEG:R.COMP<EB.COM.SUB.DIVISION.CODE>
            Y.LCY.ACCT = LCCY:CCY:Y.AL.CATEG:R.COMP<EB.COM.SUB.DIVISION.CODE>
            CALL OCOMO("Y.FCY.ACCT :":Y.FCY.ACCT)
            CALL OCOMO("Y.LCY.ACCT :":Y.LCY.ACCT)
            CALL OCOMO("Y.CCY :":Y.CCY)
            CALL OCOMO("DC.MARKER :":DC.MARKER)
            Y.CCY = CCY
            IF DC.MARKER = 'D' THEN
                DEBIT.POS.ACCT = Y.FCY.ACCT
                CREDIT.POS.ACCT = Y.LCY.ACCT
            END ELSE
                DEBIT.POS.ACCT = Y.LCY.ACCT
                CREDIT.POS.ACCT = Y.FCY.ACCT
            END
            CALL OCOMO("DEBIT.POS.ACCT :":DEBIT.POS.ACCT)
            CALL OCOMO("CREDIT.POS.ACCT :":CREDIT.POS.ACCT)
            POS.ACCT.OFS = "POS.ACCT.CORRECTION,/I/PROCESS//0,//":CO.CODE:",,DEBIT.POS.ACCT::=":DEBIT.POS.ACCT:",CREDIT.POS.ACCT::=":CREDIT.POS.ACCT:",CURRENCY::=":CCY:",AMOUNT.FCY::=":AMT.FCY
            CALL OCOMO('POS.ACCT.OFS MSG:':POS.ACCT.OFS)
            CALL OFS.GLOBUS.MANAGER('TESTOFS',POS.ACCT.OFS)
            CALL OCOMO("POS.ACCT.OFS MSG RESPONSE :":POS.ACCT.OFS)
        END
    END
    RETURN
*-----------------------------------------------------------------------------
CALL.EXCHANGE:
*-----------------------------------------------------------------------------

    Y.RATE = ''
    IF NOT(AMT.LCY) THEN
        CALL MIDDLE.RATE.CONV.CHECK(AMT.FCY, CCY, Y.RATE, "1", AMT.LCY, "", "")
    END

    RETURN

*-----------------------------------------------------------------------------
CREATE.POSITION:
*-----------------------------------------------------------------------------

    IF CCY EQ LCCY THEN
        CALL OCOMO('POSITION NOT REQUIRED FOR LOCAL CURRENCY')
        RETURN
    END

    GOSUB CALL.EXCHANGE
    IF DC.MARKER = 'D' THEN
        AMT.FCY = -AMT.FCY
        AMT.LCY = -AMT.LCY
    END
    GOSUB FORM.POS.ID
    IF DC.MARKER = 'D' THEN
        AMT.FCY = -AMT.FCY
        AMT.LCY = -AMT.LCY
    END
    RETURN

*-----------------------------------------------------------------------------
FORM.POS.ID:
*-----------------------------------------------------------------------------

    POS.ID = CO.CODE:'1TR00':CCY:LCCY:Y.POS.DATE
    POS.REC = ''; POS.ERR = ''; POS.LCY.REC = ''
    READ POS.REC FROM F.POSITION,POS.ID ELSE POS.REC = '' 
       IF NOT(POS.REC) THEN
        GOSUB UPDATE.POSITION
        GOSUB UPDATE.LCY.POSITION
    END ELSE
* if Position record exists then check for the record with previous working day...
        CALL CDT('', Y.POS.DATE, '-1W')
        GOSUB FORM.POS.ID
    END

    RETURN

*-----------------------------------------------------------------------------
UPDATE.POSITION:
*-----------------------------------------------------------------------------

    POS.REC<CCY.POS.AMOUNT.1> = -AMT.FCY
    POS.REC<CCY.POS.AMOUNT.2> = AMT.LCY
    POS.REC<CCY.POS.LCY.AMOUNT> = -AMT.LCY
    POS.REC<CCY.POS.SD.AMOUNT.1, 1> = -AMT.FCY
    POS.REC<CCY.POS.SD.AMOUNT.2, 1> = AMT.LCY
    POS.REC<CCY.POS.SD.LCY.AMOUNT, 1> = -AMT.LCY
    POS.REC<CCY.POS.SYSTEM.DATE> = TODAY
    CALL OCOMO('Position record created with ID: ':POS.ID)
    WRITE POS.REC TO F.POSITION, POS.ID

    RETURN

*-----------------------------------------------------------------------------
UPDATE.LCY.POSITION:
*-----------------------------------------------------------------------------

    POS.LCY.ID = CO.CODE:'1TR00':LCCY:CCY:Y.POS.DATE
    POS.LCY.REC<CCY.POS.AMOUNT.1> = AMT.LCY
    POS.LCY.REC<CCY.POS.AMOUNT.2> = -AMT.FCY
    POS.LCY.REC<CCY.POS.LCY.AMOUNT> = AMT.LCY
    POS.LCY.REC<CCY.POS.SD.AMOUNT.1, 1> = AMT.LCY
    POS.LCY.REC<CCY.POS.SD.AMOUNT.2, 1> = -AMT.FCY
    POS.LCY.REC<CCY.POS.SD.LCY.AMOUNT, 1> = AMT.LCY
    POS.LCY.REC<CCY.POS.SYSTEM.DATE> = TODAY
    CALL OCOMO('Position record created with ID: ':POS.LCY.ID)
    WRITE POS.LCY.REC TO F.POSITION, POS.LCY.ID

    RETURN

*-----------------------------------------------------------------------------
CET.RAISE:
*-----------------------------------------------------------------------------

    R.CET = ""
    R.CET<RE.CET.PRODUCT> = PRD
    R.CET<RE.CET.TXN.REF> = TXN.REF
    R.CET<RE.CET.CURRENCY> = CCY
    R.CET<RE.CET.CURRENCY.MARKET> = MKT
    R.CET<RE.CET.TYPE> = ASST.TYPE
    IF CCY NE LCCY THEN
        BEGIN CASE
        CASE DC.MARKER EQ 'C'
            R.CET<RE.CET.LOCAL.DR> = -AMT.LCY
            R.CET<RE.CET.FOREIGN.DR> = -AMT.FCY
        CASE DC.MARKER EQ 'D'
            R.CET<RE.CET.LOCAL.CR> = AMT.LCY
            R.CET<RE.CET.FOREIGN.CR> = AMT.FCY
        END CASE
        R.CET<RE.CET.EXCHANGE.RATE> = Y.RATE
    END ELSE
        BEGIN CASE
        CASE DC.MARKER EQ 'C'
            R.CET<RE.CET.LOCAL.DR> = -AMT.LCY
        CASE DC.MARKER EQ 'D'
            R.CET<RE.CET.LOCAL.CR> = AMT.LCY
        END CASE
        R.CET<RE.CET.EXCHANGE.RATE> = 1.0000
    END
    R.CET<RE.CET.TXN.CODE> = "COR"
    R.CET<RE.CET.CONSOL.KEY> = CET.KEY
    R.CET<RE.CET.SUPPRESS.POSITION> = "Y"
    IF CCY NE LCCY THEN
        PRINT
        CALL OCOMO('CET IS RAISED FOR CONSOL KEY :: ':CET.KEY:'.':ASST.TYPE:'  &&  FOR AMT :: ':AMT.FCY)
        PRINT
    END ELSE
        PRINT
        CALL OCOMO('CET IS RAISED FOR CONSOL KEY :: ':CET.KEY:'.':ASST.TYPE:'  &&  FOR AMT :: ':AMT.LCY)
        PRINT
    END
    GOSUB GENERATE.ID

    WRITE R.CET TO F.CONSOL.ENT.TODAY, CET.ID
    GOSUB EOD.CONSOL.UPDATE

    RETURN

*-----------------------------------------------------------------------------
EOD.CONSOL.UPDATE:
*-----------------------------------------------------------------------------

    EOD.CONSOL.ID = ''
    EOD.CONSOL.ID = TXN.REF:'*':TODAY
    READ EOD.CONSOL.REC FROM F.EOD.CONSOL.UPDATE.DETAIL,EOD.CONSOL.ID ELSE EOD.CONSOL.REC = ''
    EOD.CONSOL.REC<-1> = PRD:'*':CET.ID
    WRITE EOD.CONSOL.REC TO F.EOD.CONSOL.UPDATE.DETAIL,EOD.CONSOL.ID
    EOD.CONSOL.REC = ''

    RETURN

*-----------------------------------------------------------------------------
UPDATE.BL.ENTRY:
*-----------------------------------------------------------------------------

* update CONSOL.UPDATE.WORK for contingent BL type

    BL.ASST.TYPE = ASST.TYPE:'BL'
    Y.CUW.ID = CET.KEY:'*':BL.ASST.TYPE:'**':TODAY:'*':ABS(TNO)
    READ R.CONSOL.UPDATE.WORK FROM F.CONSOL.UPDATE.WORK, Y.CUW.ID ELSE
        R.CONSOL.UPDATE.WORK<RE.CUW.CURRENCY> = CCY
        BEGIN CASE
        CASE DC.MARKER EQ 'C' AND CCY NE LCCY
            R.CONSOL.UPDATE.WORK<RE.CUW.CREDIT.MVMT> = AMT.FCY
        CASE DC.MARKER EQ 'D' AND CCY NE LCCY
            R.CONSOL.UPDATE.WORK<RE.CUW.DEBIT.MVMT> = AMT.FCY
        CASE DC.MARKER EQ 'C' AND CCY EQ LCCY
            R.CONSOL.UPDATE.WORK<RE.CUW.CREDIT.MVMT> = AMT.LCY
        CASE DC.MARKER EQ 'D' AND CCY EQ LCCY
            R.CONSOL.UPDATE.WORK<RE.CUW.DEBIT.MVMT> = AMT.LCY
        END CASE
    END
    WRITE R.CONSOL.UPDATE.WORK TO F.CONSOL.UPDATE.WORK, Y.CUW.ID

    RETURN

*-----------------------------------------------------------------------------
UPDATE.ECB:
*-----------------------------------------------------------------------------


    R.EB.CONTRACT.BALANCES = ''; POS = ''; AMT.FCY.NEG='' ; AMT.LCY.NEG=''
    READU R.EB.CONTRACT.BALANCES FROM F.EB.CONTRACT.BALANCES,TXN.REF ELSE R.EB.CONTRACT.BALANCES = ''
    IF R.EB.CONTRACT.BALANCES THEN

        CON.CCY = R.EB.CONTRACT.BALANCES<ECB.CURRENCY>
        LOCATE ASST.TYPE IN R.EB.CONTRACT.BALANCES<ECB.CURR.ASSET.TYPE,1> SETTING POS THEN
            BEGIN CASE
            CASE DC.MARKER EQ 'D' AND CCY NE LCCY
                R.EB.CONTRACT.BALANCES<ECB.CREDIT.MVMT,POS,1> + = AMT.FCY
            CASE DC.MARKER EQ 'C' AND CCY NE LCCY
                R.EB.CONTRACT.BALANCES< ECB.DEBIT.MVMT,POS,1> + = -AMT.FCY
            CASE DC.MARKER EQ 'D' AND CCY EQ LCCY
                R.EB.CONTRACT.BALANCES<ECB.CREDIT.MVMT,POS,1> + = AMT.LCY
            CASE DC.MARKER EQ 'C' AND CCY EQ LCCY
                R.EB.CONTRACT.BALANCES< ECB.DEBIT.MVMT,POS,1> + = -AMT.LCY
            END CASE
            IF AMT.FCY THEN
                CALL OCOMO("EB.CONTRACT.BALANCES ":TXN.REF:" updated with amount ":AMT.FCY:" for asset type ":ASST.TYPE)
            END ELSE
                CALL OCOMO("EB.CONTRACT.BALANCES ":TXN.REF:" updated with amount ":AMT.LCY:" for asset type ":ASST.TYPE)
            END
        END

* To update ECB from contingent BL asset type
        IF NOT(NON.CONTINGENT) AND SELF.BAL EQ 'Y' THEN
            LOCATE BL.ASST.TYPE IN R.EB.CONTRACT.BALANCES<ECB.CURR.ASSET.TYPE,1> SETTING POS THEN
                BEGIN CASE
                CASE DC.MARKER EQ 'D' AND CCY NE LCCY
                    R.EB.CONTRACT.BALANCES< ECB.DEBIT.MVMT,POS,1> + = -AMT.FCY
                CASE DC.MARKER EQ 'C' AND CCY NE LCCY
                    R.EB.CONTRACT.BALANCES<ECB.CREDIT.MVMT,POS,1> + = AMT.FCY
                CASE DC.MARKER EQ 'D' AND CCY EQ LCCY
                    R.EB.CONTRACT.BALANCES< ECB.DEBIT.MVMT,POS,1> + = -AMT.LCY
                CASE DC.MARKER EQ 'C' AND CCY EQ LCCY
                    R.EB.CONTRACT.BALANCES<ECB.CREDIT.MVMT,POS,1> + = AMT.LCY
                END CASE
                IF AMT.FCY THEN
                    CALL OCOMO("EB.CONTRACT.BALANCES ":TXN.REF:" updated with amount ":AMT.FCY:" for asset type ":ASST.TYPE)
                END ELSE
                    CALL OCOMO("EB.CONTRACT.BALANCES ":TXN.REF:" updated with amount ":AMT.LCY:" for asset type ":ASST.TYPE)
                END
            END

        END


        IF AMT.FCY THEN
            CALL OCOMO("EB.CONTRACT.BALANCES ":TXN.REF:" updated with amount ":AMT.FCY:" for asset type ":ASST.TYPE)
        END ELSE
            CALL OCOMO("EB.CONTRACT.BALANCES ":TXN.REF:" updated with amount ":AMT.LCY:" for asset type ":ASST.TYPE)
        END
        WRITE R.EB.CONTRACT.BALANCES TO F.EB.CONTRACT.BALANCES, TXN.REF
    END
    RELEASE F.EB.CONTRACT.BALANCES, TXN.REF

    RETURN

*-----------------------------------------------------------------------------
UPDATE.RCC:
*-----------------------------------------------------------------------------

    R.RE.CONSOL.CONTRACT = ''
    READ R.RE.CONSOL.CONTRACT FROM F.RE.CONSOL.CONTRACT, CET.KEY THEN
        FINDSTR TXN.REF IN R.RE.CONSOL.CONTRACT SETTING CONTRACT.POS THEN
            DEL R.RE.CONSOL.CONTRACT<CONTRACT.POS>
            IF NOT(R.RE.CONSOL.CONTRACT) THEN
                CALL OCOMO(TXN.REF:' deleted from ':FN.RE.CONSOL.CONTRACT:' and ':CET.KEY:' deleted')
                DELETE F.RE.CONSOL.CONTRACT, CET.KEY
            END ELSE
                CALL OCOMO(TXN.REF:' deleted from ':FN.RE.CONSOL.CONTRACT:' record ':CET.KEY)
                WRITE R.RE.CONSOL.CONTRACT TO F.RE.CONSOL.CONTRACT, CET.KEY
            END
        END
    END

    RETURN

*-----------------------------------------------------------------------------
GENERATE.ID:
*-----------------------------------------------------------------------------

    UNIQ.TIME = ''
    CALL ALLOCATE.UNIQUE.TIME(UNIQ.TIME)
    DT = DATE(); CET.ID = ''
    READ DUMMY.REC FROM F.CONSOL.ENT.TODAY, DT:UNIQ.TIME THEN
        UNIQ.TIME = FIELD(UNIQ.TIME,'.',1):'.99'
    END
    CET.ID = DT:UNIQ.TIME

    RETURN
