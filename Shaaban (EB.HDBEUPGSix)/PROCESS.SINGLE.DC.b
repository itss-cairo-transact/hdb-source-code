*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE PROCESS.SINGLE.DC
*** SAVEDLIST FORMAT ACCOUNT.ID#AMOUNT

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.EB.CONTRACT.BALANCES

    GOSUB INIT
    GOSUB PROCESS

    RETURN


INIT:
    FN.SL = "&SAVEDLISTS&"
    F.SL = ''
    CALL OPF(FN.SL,F.SL)
    SL.ID = "DC.KEYS.CORR";
    SL.REC = '';
    SL.ERR = ''

    F.ECB = ''
    FN.ECB = 'F.EB.CONTRACT.BALANCES'
    CALL OPF(FN.ECB,F.ECB)


    RETURN


PROCESS:
    CALL F.READ(FN.SL,SL.ID,CORR.LIST,F.SL,SL.ERR)
    LOOP
        REMOVE CORR.ID FROM CORR.LIST SETTING CORR.POS
    WHILE CORR.ID:CORR.POS
        AC.ID = FIELD(CORR.ID,'#',1)
        CORR.AMT = FIELD(CORR.ID,'#',2)
        CALL F.READ(FN.ECB,AC.ID,R.ECB,F.ECB,ECB.ERR)
        IF NOT(ECB.ERR) THEN
            CONSOL.KEY = R.ECB<ECB.CONSOL.KEY>
            CONTRACT.ID = AC.ID
            COMPANY.CODE = R.ECB<ECB.CO.CODE>
            CUST.ID = R.ECB<ECB.CUSTOMER>
            PROCESS.ID = CONSOL.KEY:"*":CONTRACT.ID:"*":CORR.AMT:"*":COMPANY.CODE:"*":AC.ID:"*":CUST.ID
            CALL LOAD.COMPANY(COMPANY.CODE)
            CALL POST.SINGLE.DC(PROCESS.ID)
        END ELSE
            CRT "Cannot read ECB record : ":AC.ID
        END
    REPEAT

    RETURN

