*---------------------------------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>1532</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE RE.EXTRACT.BASIC.INFO
*---------------------------------------------------------------------------------------------------
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.COMPANY
    $INSERT I_F.DATES
    $INSERT I_F.EB.SYSTEM.SUMMARY
    $INSERT I_F.EB.JOURNAL.SUMMARY
    $INSERT I_F.CONSOLIDATE.ASST.LIAB
    $INSERT I_F.CONSOLIDATE.PRFT.LOSS
    $INSERT I_F.POSITION
    $INSERT I_F.ACCOUNT
    $INSERT I_F.EB.CONTRACT.BALANCES
    $INSERT I_F.RE.STAT.LINE.BAL

*---------------------------------------------------------------------------------------------------


*   Code changes done to make the ID format UNIQUE
*   20/07/2015 Moved the invalid option to case statement. Updated Version text in header.
*   13/08/2015 Condition added to extract ESS based on user input
*   01/09/2015 RE.CRF flat file extraction added
*   20/10/2015 Invalid output message relating to CAL/CPL extraction removed.
*   21/04/2016 Entry details column order modified to be compatible with SQL reconciliation

    GOSUB GET.INPUT
    IF COMI = '' OR COMI = 99 THEN RETURN
    GOSUB PROCESS
    GOSUB WRITE.SAVEDLISTS

    RETURN

*---------------------------------------------------------------------------------------------------
GET.INPUT:
*---------------------------------------------------------------------------------------------------
    PRINT @(5,4) : "You are running RE EXTRACTOR Version 1.0"
    PRINT @(5,6) : "1. ESS(EB.SYSTEM.SUMMARY) & EJS(EB.JOURNAL.SUMMARY)"
    PRINT @(5,7) : "2. RE.CRF.REPORT_NAME (RE.CRF flat file extraction)"
    PRINT @(5,8) : "3. STMT.ENTRY"
    PRINT @(5,9) : "4. RE.CONSOL.SPEC.ENTRY"
    PRINT @(5,10) : "5. CATEG.ENTRY"
    PRINT @(5,11) : "6. PMH(POS.MVMT.HIST)"
    PRINT @(5,12) : "7. CAL/CPL"
    PRINT @(5,13) : "8. POSITION"
    PRINT @(5,14) : "9. ACCOUNT"
    PRINT @(5,15) : "10. EB.CONTRACT.BALANCES"
    PRINT @(5,16) : "11. RE.STAT.LINE.BAL"
    PRINT @(5,17) : "99. EXIT"

    CALL TXTINP('Choose options ', 8, 22,32, 'ANY')

    IF COMI = 99 THEN RETURN

    BEGIN CASE

    CASE COMI = 1
        APP = 'ESS'
        FN.ESS = "F.EB.SYSTEM.SUMMARY"
        F.ESS = ""
        CALL OPF(FN.ESS,F.ESS)

*    CASE COMI = 299
*        APP = 'EJS'
        FN.EJS = "F.EB.JOURNAL.SUMMARY"
        F.EJS = ""
        CALL OPF(FN.EJS,F.EJS)
    CASE COMI = 2
        APP = 'RE.CRF'
        ENTRY.FILE = 'RE.CRF'
    CASE COMI = 3
        APP = 'STMT'
        ENTRY.FILE = 'STMT.ENTRY'
    CASE COMI = 4
        APP = 'SPEC'
        ENTRY.FILE = 'RE.CONSOL.SPEC.ENTRY'
    CASE COMI = 5
        APP = 'CATEG'
        ENTRY.FILE = 'CATEG.ENTRY'
    CASE COMI = 6
        APP = 'PMH'
        ENTRY.FILE = 'POS.MVMT.HIST'
    CASE COMI = 7
        APP = 'CAL.CPL'
        FN.CAL = "F.CONSOLIDATE.ASST.LIAB"
        F.CAL = ""
        CALL OPF(FN.CAL,F.CAL)
        FN.CPL = "F.CONSOLIDATE.PRFT.LOSS"
        F.CPL = ""
        CALL OPF(FN.CPL,F.CPL)
    CASE COMI = 8
        APP = 'POSITION'
        FN.POSITION = "F.POSITION"
        F.POSITION = ""
        CALL OPF(FN.POSITION,F.POSITION)
    CASE COMI = 9
        APP = 'ACCOUNT'
        FN.ACCOUNT = "F.ACCOUNT"
        F.ACCOUNT = ""
        CALL OPF(FN.ACCOUNT,F.ACCOUNT)
        FN.RE.CONSOL.CONTRACT = 'F.RE.CONSOL.CONTRACT'
        F.RE.CONSOL.CONTRACT = ''
        CALL OPF(FN.RE.CONSOL.CONTRACT,F.RE.CONSOL.CONTRACT)
    CASE COMI = 10
        APP = 'ECB'
        FN.RE.CONSOL.CONTRACT = 'F.RE.CONSOL.CONTRACT'
        F.RE.CONSOL.CONTRACT = ''
        CALL OPF(FN.RE.CONSOL.CONTRACT,F.RE.CONSOL.CONTRACT)
        FN.EB.CONTRACT.BALANCES = 'F.EB.CONTRACT.BALANCES'
        F.EB.CONTRACT.BALANCES = ''
        CALL OPF(FN.EB.CONTRACT.BALANCES,F.EB.CONTRACT.BALANCES)
    CASE COMI = 11
        APP = 'LINEBAL'
        FN.RE.STAT.LINE.BAL = 'F.RE.STAT.LINE.BAL'
        F.RE.STAT.LINE.BAL = ''
        CALL OPF(FN.RE.STAT.LINE.BAL,F.RE.STAT.LINE.BAL)
    CASE 1
        PRINT "Invalid option. Enter one of the displayed options "
        SLEEP(2)
        GOSUB GET.INPUT

    END CASE

    FN.BP = "PACS.BP":FM:'NO.FATAL.ERROR'
    F.BP = ""
    CALL OPF(FN.BP,F.BP)

    IF ETEXT THEN   ;* If does not exist, then create a file
        EXECUTE "CREATE.FILE PACS.BP TYPE=UD"

        FN.BP = "PACS.BP"
        F.BP = ""
        CALL OPF(FN.BP,F.BP)
    END

    RETURN

*---------------------------------------------------------------------------------------------------
PROCESS:
*---------------------------------------------------------------------------------------------------
    DATE.ID = R.DATES(EB.DAT.LAST.WORKING.DAY) ; WRITE.XL = '' ;

    BEGIN CASE

    CASE APP = 'ESS'
        GOSUB EXTRACT.ESS
        GOSUB WRITE.SAVEDLISTS
        APP = 'EJS'
        GOSUB EXTRACT.EJS
    CASE APP = 'RE.CRF'
        GOSUB EXTRACT.RE.CRF
    CASE APP = 'STMT'
        ENTRY.FILE = 'STMT.ENTRY' ;
        A = 0; B= 12 ; C = 17 ; D = 23 ; E = 4 ; Z = 11 ; G = 25 ; H = 3 ; I = 13 ; J = 39 ; K = 24 ; L1 = 2 ; M = 36 ; Y = 1 ;
        WRITE.XL =  '@ID|CURRENCY|OUR.REFERENCE|TRANS.REFERENCE|TRANSACTION.CODE|VALUE.DATE|BOOKING.DATE|AMOUNT.LCY|AMOUNT.FCY|CONSOL.KEY|SYSTEM.ID|COMPANY.CODE|CRF.TYPE|ACCOUNT.NUMBER'
        GOSUB EXTRACT.ENTRY
    CASE APP = 'SPEC'
        ENTRY.FILE = 'RE.CONSOL.SPEC.ENTRY' ;
        A = 0 ; B = 12; C = 17 ; D = 23 ; E = 4 ; Z = 11 ; G = 25 ; H = 3 ; I = 13 ; J = 5 ; K = 24 ; L1 = 2 ;
        WRITE.XL =  '@ID|CURRENCY|OUR.REFERENCE|TRANS.REFERENCE|TRANSACTION.CODE|VALUE.DATE|BOOKING.DATE|AMOUNT.LCY|AMOUNT.FCY|CONSOL.KEY.TYPE|SYSTEM.ID|COMPANY.CODE'
        GOSUB EXTRACT.ENTRY
    CASE APP = 'CATEG'
        ENTRY.FILE = 'CATEG.ENTRY' ;
        A = 0 ; B = 12 ; C = 17 ; D = 23 ; E = 4 ; Z = 11 ; G = 25 ; H = 3 ; I = 13 ; J = 56; K = 24 ; L1 = 2; M = 7 ;
        WRITE.XL =  '@ID|CURRENCY|OUR.REFERENCE|TRANS.REFERENCE|TRANSACTION.CODE|VALUE.DATE|BOOKING.DATE|AMOUNT.LCY|AMOUNT.FCY|CONSOL.KEY|SYSTEM.ID|COMPANY.CODE|PL.CATEGORY'
        GOSUB EXTRACT.ENTRY
    CASE APP = 'PMH'
        ENTRY.FILE = 'POS.MVMT.HIST' ;
        A = 0 ; B = 12; C = 17 ; D = 23 ; E = 4 ; Z = 11 ; G = 25 ; H = 3 ; I = 13; J = 14; K = 24 ; L1 = 2 ;
        WRITE.XL = '@ID|CURRENCY|OUR.REFERENCE|TRANS.REFERENCE|TRANSACTION.CODE|VALUE.DATE|BOOKING.DATE|AMOUNT.LCY|AMOUNT.FCY|EXCHANGE.RATE|SYSTEM.ID|COMPANY.CODE'
        GOSUB EXTRACT.ENTRY
    CASE APP = 'CAL.CPL'
        APP = "CAL"
        GOSUB EXTRACT.CAL
        GOSUB WRITE.SAVEDLISTS
        APP = "CPL"
        GOSUB EXTRACT.CPL
    CASE APP = 'POSITION'
        GOSUB EXTRACT.POSITION
    CASE APP = 'ACCOUNT'
        GOSUB EXTRACT.ACCOUNT
    CASE APP = 'ECB'
        GOSUB EXTRACT.ECB
    CASE APP = 'LINEBAL'
        GOSUB EXTRACT.LINE.BAL
    END CASE


    RETURN
*---------------------------------------------------------------------------------------------------
WRITE.SAVEDLISTS:
*---------------------------------------------------------------------------------------------------
    IF WRITE.XL<2> THEN
        WRITE WRITE.XL TO F.BP,APP:"-":DATE.ID:FIELD(SYSTEM(12),'.',1):'.csv'
        WRITE.XL = '' ;

        CRT @(-1)
        PRINT @(10,8) : "EXTRACTION COMPLETED. Extract the record " :APP:"-":DATE.ID:".csv": " from PACS.BP and send us"
        SLEEP(2)
    END ELSE
        CRT @(-1)
        PRINT @(10,8) : " Warning : EXTRACTOR Completed. But NO Data available for the given input. "
        SLEEP(2)
    END


    RETURN
*---------------------------------------------------------------------------------------------------
EXTRACT.ESS:
*---------------------------------------------------------------------------------------------------
    CRT @(-1)
    PRINT @(5,7) : "1. Enter the Problem START Date: "
    PRINT @(5,11) : "Note: If unsure about problem start date, please enter first date of current year"
    CALL TXTINP('Enter date in YYYYMMDD format', 8, 22,32, 'D')
    DT=COMI
    CALL CDT('',DT,'-01W')

    WRITE.XL = 'ID|AL.CCY.MKT|AL.CCY|AL.CCY.BAL.AMT|AL.LCL.BAL|AL.CCY.POSN|AL.LCL.POS|AL.CCY.EXCEP|AL.LCY.EXCEP|AL.TOTAL.LCY|PL.TOTAL.LCY|EXCEPT.AMT.LCY'
*    SEL.CMD = 'SSELECT ':FN.ESS:' BY.DSND @ID'    ;*  WITH @ID LIKE ...2014... BY.DSND @ID'
    SEL.CMD = 'SELECT ':FN.ESS:' WITH EVAL "FIELD(@ID,'
    SEL.CMD:= "'-',2,1)"
    SEL.CMD:= '" GE '
    SEL.CMD:= DT:' BY.DSND @ID'

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,SEL.ERR)
    LOOP
        REMOVE ESS.ID FROM SEL.LIST SETTING ID.ESS
    WHILE ESS.ID:ID.ESS
        READ R.REC FROM F.ESS,ESS.ID ELSE
*            CRT "Missing record ":ESS.ID:"in " : FN.ESS
            R.REC = ""
        END
        NO.OF.VAL = '' ; R.NULL = ''
        NO.OF.VAL = DCOUNT(R.REC<EB.SYSUM.AL.CCY.MKT>,@VM)
        FOR J = 1 TO NO.OF.VAL
            IF J EQ 1 THEN
                WRITE.XL<-1> = ESS.ID :'|':R.REC<EB.SYSUM.AL.CCY.MKT,J>:'|':R.REC<EB.SYSUM.AL.CCY,J>:'|':R.REC<EB.SYSUM.AL.CCY.BAL.AMT,J>:'|':R.REC<EB.SYSUM.AL.LCL.BAL,J>:'|':R.REC<EB.SYSUM.AL.CCY.POSN,J>:'|':R.REC<EB.SYSUM.AL.LCL.POS,J>:'|':R.REC<EB.SYSUM.AL.CCY.EXCEP,J>:'|':R.REC<EB.SYSUM.AL.LCY.EXCEP,J>:'|':R.REC<EB.SYSUM.AL.TOTAL.LCY>:'|':R.REC<EB.SYSUM.PL.TOTAL.LCY>:'|':R.REC<EB.SYSUM.EXCEPT.AMT.LCY> ;
            END ELSE
                WRITE.XL<-1> = ESS.ID :'|':R.REC<EB.SYSUM.AL.CCY.MKT,J>:'|':R.REC<EB.SYSUM.AL.CCY,J>:'|':R.REC<EB.SYSUM.AL.CCY.BAL.AMT,J>:'|':R.REC<EB.SYSUM.AL.LCL.BAL,J>:'|':R.REC<EB.SYSUM.AL.CCY.POSN,J>:'|':R.REC<EB.SYSUM.AL.LCL.POS,J>:'|':R.REC<EB.SYSUM.AL.CCY.EXCEP,J>:'|':R.REC<EB.SYSUM.AL.LCY.EXCEP,J>:'|':R.NULL:'|':R.NULL:'|':R.NULL ;
            END
        NEXT J
    REPEAT

    RETURN

*---------------------------------------------------------------------------------------------------
EXTRACT.EJS:
*---------------------------------------------------------------------------------------------------
*    CRT @(-1)
*    PRINT @(5,7) : "1. Enter the Problem START Date: "
*    PRINT @(5,11) : "Note: If unsure about problem start date, please enter first date of current year"
*    CALL TXTINP('Enter date in YYYYMMDD format', 8, 22,32, 'D')
*    DT=COMI
*    CALL CDT('',DT,'-01W')


    WRITE.XL = '@ID|NON.CONT.APP|NC.DEBIT.TOT|NC.CREDIT.TOT|NON.CONT.TOT|NON.CONT.BALANCE'
*    SEL.CMD = 'SSELECT ':FN.EJS:' BY.DSND @ID'    ;* WITH @ID LIKE ...2014... BY.DSND @ID'
    SEL.CMD = 'SELECT ':FN.EJS:' WITH EVAL "FIELD(@ID,'
    SEL.CMD:= "'-',2,1)"
    SEL.CMD:= '" GE '
    SEL.CMD:= DT:' BY.DSND @ID'

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,SEL.ERR)
    LOOP
        REMOVE EJS.ID FROM SEL.LIST SETTING ID.EJS
    WHILE EJS.ID:ID.EJS
        READ R.REC FROM F.EJS,EJS.ID ELSE
*            CRT "Missing record ":ESS.ID:"in " : FN.ESS
            R.REC = ""
        END
        NO.OF.VAL = '' ; R.NULL = ''
        NO.OF.VAL = DCOUNT(R.REC<EB.JS.NON.CONT.APP>,@VM)
        FOR J = 1 TO NO.OF.VAL
            IF J EQ 1 THEN
                WRITE.XL<-1> = EJS.ID :'|':R.REC<EB.JS.NON.CONT.APP,J>:'|':R.REC<EB.JS.NC.DEBIT.TOT,J>:'|':R.REC<EB.JS.NC.CREDIT.TOT,J>:'|':R.REC<EB.JS.NON.CONT.TOT,J>:'|':R.REC<EB.JS.NON.CONT.BALANCE> ;
            END ELSE
                WRITE.XL<-1> = EJS.ID :'|':R.REC<EB.JS.NON.CONT.APP,J>:'|':R.REC<EB.JS.NC.DEBIT.TOT,J>:'|':R.REC<EB.JS.NC.CREDIT.TOT,J>:'|':R.REC<EB.JS.NON.CONT.TOT,J>:'|':R.NULL ;
            END
        NEXT J
    REPEAT


    RETURN

*---------------------------------------------------------------------------------------------------
EXTRACT.ENTRY:
*---------------------------------------------------------------------------------------------------
    CRT @(-1)
    PRINT @(10,8)  : "1.BOOKING.DATE "
    PRINT @(10,9)  : "2.VALUE.DATE"
    PRINT @(10,10) : "3.CONSOL.KEY"
    PRINT @(10,11) : "4.OUR.REFERENCE"
    PRINT @(10,12) : "5.ACCOUNT"


    CALL TXTINP('Choose selection field ', 8, 22,32, 'ANY')

    IF COMI = '' THEN
        PRINT "Invalid option.Re-run the tool again"
        SLEEP(2)
        RETURN
    END

    BEGIN CASE

    CASE COMI = 1
        COMI = 'BOOKING.DATE'
    CASE COMI = 2
        COMI = 'VALUE.DATE'
    CASE COMI = 3
        COMI = 'CONSOL.KEY'
    CASE COMI = 4
        COMI = 'OUR.REFERENCE'
    CASE COMI = 5
        COMI = 'ACCOUNT.NUMBER'
    END CASE

    PRINT "Enter the value : "
    INPUT DATE.ID
    IF DATE.ID = '' THEN RETURN

    FN.ENTRY.FILE= 'F.':ENTRY.FILE
    F.ENTRY.FILE = ''
    CALL OPF(FN.ENTRY.FILE,F.ENTRY.FILE)

    SEL.CMD = 'SELECT ':FN.ENTRY.FILE: " WITH ":COMI:" EQ ": DATE.ID
    EXECUTE SEL.CMD CAPTURING SEL.OUT
    READLIST SEL.LIST ELSE SEL.LIST = ''
    LOOP
        REMOVE ENTRY.ID FROM SEL.LIST SETTING ENT.POS
    WHILE ENTRY.ID:ENT.POS

        READ R.REC FROM F.ENTRY.FILE, ENTRY.ID THEN
            WRITE.XL<-1> = ENTRY.ID :'|': R.REC<B> :'|': R.REC<C> :'|': R.REC<D> :'|': R.REC<E> :'|': R.REC<Z> :'|': R.REC<G> :'|': R.REC<H> :'|': R.REC<I> :'|': R.REC<J> :'|': R.REC<K> :'|': R.REC<L1>:'|': R.REC<M>:'|': R.REC<Y>
        END
    REPEAT


    RETURN
*
*---------------------------------------------------------------------------------------------------
EXTRACT.CAL:
*---------------------------------------------------------------------------------------------------
    WRITE.XL = '@ID|DATE.LAST.UPDATE|K.TYPE|CURRENCY|BALANCE|CREDIT.MOVEMENT|DEBIT.MOVEMENT|LOCAL.BALANCE|LOCAL.DEBIT.MVE|LOCAL.CREDT.MVE'
    SEL.CMD = 'SELECT ':FN.CAL
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,SEL.ERR)
    LOOP
        REMOVE CAL.ID FROM SEL.LIST SETTING ID.CAL
    WHILE CAL.ID:ID.CAL
        READ R.REC FROM F.CAL,CAL.ID ELSE
            R.REC = ""
        END
        NO.OF.VAL = '' ; R.NULL = ''
        NO.OF.VAL = DCOUNT(R.REC<RE.ASL.TYPE>,@VM)
        FOR J = 1 TO NO.OF.VAL
*            IF J EQ 1 THEN
            WRITE.XL<-1> = CAL.ID:'|':R.REC<RE.ASL.DATE.LAST.UPDATE>:'|':R.REC<RE.ASL.TYPE,J>:'|':R.REC<RE.ASL.CURRENCY>:'|':R.REC<RE.ASL.BALANCE,J>:'|':R.REC<RE.ASL.CREDIT.MOVEMENT,J>:'|':R.REC<RE.ASL.DEBIT.MOVEMENT,J>:'|':R.REC<RE.ASL.LOCAL.BALANCE,J>:'|':R.REC<RE.ASL.LOCAL.DEBIT.MVE,J>:'|':R.REC<RE.ASL.LOCAL.CREDT.MVE,J>
*            END ELSE
*                WRITE.XL<-1> = CAL.ID:'|':R.REC<RE.ASL.DATE.LAST.UPDATE>:'|':R.REC<RE.ASL.K.TYPE>:'|':R.REC<RE.ASL.CURRENCY>:'|':R.REC<RE.ASL.BALANCE>:'|':R.REC<RE.ASL.CREDIT.MOVEMENT>:'|':R.REC<RE.ASL.DEBIT.MOVEMENT>:'|':R.REC<RE.ASL.LOCAL.BALANCE>:'|':R.REC<RE.ASL.LOCAL.DEBIT.MVE>:'|':R.REC<RE.ASL.LOCAL.CREDT.MVE>
*            END
        NEXT J

    REPEAT

*    IF WRITE.XL<2> THEN WRITE WRITE.XL TO F.BP,APP:"-":DATE.ID:'.csv' ; WRITE.XL = ''


    RETURN

*---------------------------------------------------------------------------------------------------
EXTRACT.CPL:
*---------------------------------------------------------------------------------------------------
    WRITE.XL = 'CPL.ID|DATE.LAST.UPDATE|CURRENCY|BALANCE|DEBIT.MOVEMENT|CREDIT.MOVEMENT|BALANCE.YTD|CCY.BALANCE|CCY.DEBIT.MVE|CCY.CREDT.MVE|CCY.BALANCE.YTD'
    SEL.CMD = 'SELECT ':FN.CPL
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,SEL.ERR)
    LOOP
        REMOVE CPL.ID FROM SEL.LIST SETTING ID.CPL
    WHILE CPL.ID:ID.CPL
        READ R.REC FROM F.CPL,CPL.ID ELSE
*            CRT "Missing record ":ESS.ID:"in " : FN.ESS
            R.REC = ""
        END
        NO.OF.VAL = '' ; R.NULL = ''
        NO.OF.VAL = DCOUNT(R.REC<RE.PTL.CURRENCY>,@VM)
        FOR J = 1 TO NO.OF.VAL
*            IF J EQ 1 THEN
            WRITE.XL<-1> = CPL.ID:'|':R.REC<RE.PTL.DATE.LAST.UPDATE>:'|':R.REC<RE.PTL.CURRENCY,J>:'|':R.REC<RE.PTL.BALANCE,J>:'|':R.REC<RE.PTL.DEBIT.MOVEMENT,J>:'|':R.REC<RE.PTL.CREDIT.MOVEMENT,J>:'|':R.REC<RE.PTL.BALANCE.YTD,J>:'|':R.REC<RE.PTL.CCY.BALANCE,J>:'|':R.REC<RE.PTL.CCY.DEBIT.MVE,J>:'|':R.REC<RE.PTL.CCY.CREDT.MVE,J>:'|':R.REC<RE.PTL.CCY.BALANCE.YTD,J>
*            END ELSE
*                WRITE.XL<-1> = CAL.ID:'|':R.REC<RE.ASL.DATE.LAST.UPDATE>:'|':R.REC<RE.ASL.K.TYPE>:'|':R.REC<RE.ASL.CURRENCY>:'|':R.REC<RE.ASL.BALANCE>:'|':R.REC<RE.ASL.CREDIT.MOVEMENT>:'|':R.REC<RE.ASL.DEBIT.MOVEMENT>:'|':R.REC<RE.ASL.LOCAL.BALANCE>:'|':R.REC<RE.ASL.LOCAL.DEBIT.MVE>:'|':R.REC<RE.ASL.LOCAL.CREDT.MVE>
*            END
        NEXT J
    REPEAT

*    IF WRITE.XL<2> THEN WRITE WRITE.XL TO F.BP,APP:"-":DATE.ID:'.csv' ; WRITE.XL = ''

    RETURN
*---------------------------------------------------------------------------------------------------
RE.STAT.HEAD.VALIDATION:
*---------------------------------------------------------------------------------------------------

    FN.HEAD = "F.RE.STAT.REPORT.HEAD"
    F.HEAD = ""
    CALL OPF(FN.HEAD,F.HEAD)
    HEAD.ERR =''
    ID.HEAD = COMI
    R.HEAD=''
    CALL F.READ(FN.HEAD,ID.HEAD,R.HEAD,F.HEAD,HEAD.ERR)
    IF HEAD.ERR THEN
        CRT @(-1)
        PRINT @(5,6) : "INVALID REPORT NAME ENTERED :":ID.HEAD
        PRINT @(5,8) : "(Note: It should be a valid record in RE.STAT.REPORT.HEAD File)"
        SLEEP(4)
        GOTO GET.INPUT
    END


    RETURN
*---------------------------------------------------------------------------------------------------
EXTRACT.RE.CRF:
*---------------------------------------------------------------------------------------------------

    CRT @(-1)
    CALL TXTINP('Input CRB REPORT NAME  (eg: MBGL, MBPL etc)', 8,25,75, 'ANY')
    RS.HEAD = COMI
    GOSUB RE.STAT.HEAD.VALIDATION

    WRITE.XL = 'RE.CRF.ID | CURRENCY | LOCAL.BALANCE | FOREIGN.BALANCE | DEAL.BALANCE | DEAL.LCY.BALANCE | DEAL.VALUE.DATE | DEBIT.MVMT | DEBIT.LCY.MVMT | CREDIT.MVMT | CREDIT.LCY.MVMT'
    FN.RE.CRF = 'F.RE.CRF.':ID.HEAD:FM:'NO.FATAL.ERROR'
    F.RE.CRF = ''
    CALL OPF(FN.RE.CRF,F.RE.CRF)
    IF ETEXT THEN
        CRT @(-1)
        PRINT @(5,6) : " RE.CRF FLAT FILE NOT CREATED FOR THE GIVEN REPORT "
        PRINT @(5,8) : " Kindly run the RE.RETURN.EXTRACT online service for the report:":ID.HEAD
        SLEEP(5)
        ETEXT = ''
        TEXT = ''
        CRT @(-1)
        GOTO GET.INPUT
    END ELSE
        SEL.CMD = 'SELECT ':FN.RE.CRF
        CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,SEL.ERR)
        LOOP
            REMOVE RE.CRF.ID FROM SEL.LIST SETTING ID.POS
        WHILE RE.CRF.ID:ID.POS
            READ R.REC FROM F.RE.CRF,RE.CRF.ID THEN
                WRITE.XL<-1> = RE.CRF.ID:'|': R.REC<1> :'|': R.REC<4> :'|': R.REC<5> :'|': R.REC<11> :'|': R.REC<12> :'|': R.REC<14> :'|': R.REC<20> :'|': R.REC<21> :'|': R.REC<22> :'|': R.REC<23>
            END ELSE
                CRT "Missing record ":RE.CRF.ID:"in " : FN.RE.CRF
                R.REC = ""
                CONTINUE;
            END

        REPEAT
    END


    RETURN
*---------------------------------------------------------------------------------------------------
EXTRACT.POSITION:
*---------------------------------------------------------------------------------------------------
    WRITE.XL = 'POSITION.ID | AMOUNT.1 | AMOUNT.2 | LCY.AMOUNT | TXN.REF.NO | SYSTEM.DATE | SD.AMOUNT.1 | SD.AMOUNT.2 | SD.LCY.AMOUNT'
    SEL.CMD = 'SELECT ':FN.POSITION
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,SEL.ERR)
    LOOP
        REMOVE POS.ID FROM SEL.LIST SETTING ID.POS
    WHILE POS.ID:ID.POS
        READ R.REC FROM F.POSITION,POS.ID ELSE
            CRT "Missing record ":POS.ID:"in " : FN.POSITION
            R.REC = ""
        END
        NO.OF.VAL = '' ; R.NULL = ''
        NO.OF.VAL = DCOUNT(R.REC<CCY.POS.SYSTEM.DATE>,@VM)
        FOR J = 1 TO NO.OF.VAL
            IF J EQ 1 THEN
                WRITE.XL<-1> = POS.ID :'|':  R.REC<CCY.POS.AMOUNT.1,J> :'|': R.REC<CCY.POS.AMOUNT.2,J> :'|': R.REC<CCY.POS.LCY.AMOUNT,J> :'|': R.REC<CCY.POS.TXN.REF.NO,J> :'|': R.REC<CCY.POS.SYSTEM.DATE,J> :'|': R.REC<CCY.POS.SD.AMOUNT.1,J> :'|': R.REC<CCY.POS.SD.AMOUNT.2,J> :'|': R.REC<CCY.POS.SD.LCY.AMOUNT,J>
            END ELSE
                WRITE.XL<-1> = POS.ID :'|':  R.NULL :'|': R.NULL :'|': R.NULL :'|': R.NULL :'|': R.REC<CCY.POS.SYSTEM.DATE,J> :'|': R.REC<CCY.POS.SD.AMOUNT.1,J> :'|': R.REC<CCY.POS.SD.AMOUNT.2,J> :'|': R.REC<CCY.POS.SD.LCY.AMOUNT,J>
            END
        NEXT J
    REPEAT

    RETURN
*---------------------------------------------------------------------------------------------------
EXTRACT.ACCOUNT:
*---------------------------------------------------------------------------------------------------
    CRT @(-1)
    CALL TXTINP('Input CAL key ', 8,25,75, 'ANY')
    CAL.KEY = COMI

    WRITE.XL = 'ID|ACCR.DR.AMOUNT|ACCR.DR.CATEG|ACCR.DR.TRANS|ACCR.CR.AMOUNT|ACCR.CR.CATEG|ACCR.CR.TRANS|CURRENCY|OPEN.ACTUAL.BAL|OPEN.CLEARED.BAL|ONLINE.ACTUAL.BAL|ONLINE.CLEARED.BAL|WORKING.BALANCE|CO.CODE|DATE.LAST.UPDATE'

    READ R.RE.CONSOL.CONTRACT FROM F.RE.CONSOL.CONTRACT, CAL.KEY THEN
        NO.OF.ACCOUNT = DCOUNT(R.RE.CONSOL.CONTRACT,@FM)
        FOR I = 1 TO NO.OF.ACCOUNT
            READ R.ACCOUNT FROM F.ACCOUNT, R.RE.CONSOL.CONTRACT<I> ELSE CRT 'Account record ':R.RE.CONSOL.CONTRACT<I>:' is Missing'
            WRITE.XL<-1> = R.RE.CONSOL.CONTRACT<I>:'|':R.ACCOUNT<AC.ACCR.DR.AMOUNT>:'|':R.ACCOUNT<AC.ACCR.DR.CATEG>:'|':R.ACCOUNT<AC.ACCR.DR.TRANS>:'|':R.ACCOUNT<AC.ACCR.CR.AMOUNT>:'|':R.ACCOUNT<AC.ACCR.CR.CATEG>:'|':R.ACCOUNT<AC.ACCR.CR.TRANS>:'|':R.ACCOUNT<AC.CURRENCY>:'|':R.ACCOUNT<AC.OPEN.ACTUAL.BAL>:'|':R.ACCOUNT<AC.OPEN.CLEARED.BAL>:'|':R.ACCOUNT<AC.ONLINE.ACTUAL.BAL>:'|':R.ACCOUNT<AC.ONLINE.CLEARED.BAL>:'|':R.ACCOUNT<AC.WORKING.BALANCE>:'|':R.ACCOUNT<AC.CO.CODE>:'|':R.ACCOUNT<AC.DATE.LAST.UPDATE>
        NEXT I
    END
    RETURN
*---------------------------------------------------------------------------------------------------
EXTRACT.ECB:
*---------------------------------------------------------------------------------------------------
    CRT @(-1)
    CALL TXTINP('Input CAL key ', 8, 25,75, 'ANY')
    CAL.KEY = COMI

    WRITE.XL = 'ID|TYPE.SYSDATE|OPEN.BALANCE|CREDIT.MVMT|DEBIT.MVMT|CURR.ASSET.TYPE|OPEN.ASSET.TYPE|PREV.ASSET.TYPE|CONSOL.KEY|CO.CODE|APPLICATION'
    READ R.RE.CONSOL.CONTRACT FROM F.RE.CONSOL.CONTRACT, CAL.KEY THEN
        NO.OF.ACCOUNT = DCOUNT(R.RE.CONSOL.CONTRACT,@FM)
        FOR J = 1 TO NO.OF.ACCOUNT
            ECB.ID = R.RE.CONSOL.CONTRACT<J>
            READ R.EB.CONTRACT.BALANCES FROM F.EB.CONTRACT.BALANCES, ECB.ID THEN
                NO.OF.MV = DCOUNT(R.EB.CONTRACT.BALANCES<ECB.TYPE.SYSDATE>,@VM)
                FOR I = 1 TO NO.OF.MV
                    WRITE.XL<-1> = ECB.ID:'|':R.EB.CONTRACT.BALANCES<ECB.TYPE.SYSDATE,I>:'|':R.EB.CONTRACT.BALANCES<ECB.OPEN.BALANCE,I>:'|':R.EB.CONTRACT.BALANCES<ECB.CREDIT.MVMT,I>:'|':R.EB.CONTRACT.BALANCES<ECB.DEBIT.MVMT,I>:'|':R.EB.CONTRACT.BALANCES<ECB.CURR.ASSET.TYPE,I>:'|':R.EB.CONTRACT.BALANCES<ECB.OPEN.ASSET.TYPE>:'|':R.EB.CONTRACT.BALANCES<ECB.PREV.ASSET.TYPE>:'|':R.EB.CONTRACT.BALANCES<ECB.CONSOL.KEY>:'|':R.EB.CONTRACT.BALANCES<ECB.CO.CODE>:'|':R.EB.CONTRACT.BALANCES<ECB.APPLICATION>
                NEXT I
            END
        NEXT J
    END
    RETURN
*---------------------------------------------------------------------------------------------------
EXTRACT.LINE.BAL:
*---------------------------------------------------------------------------------------------------
    CRT @(-1)
    CALL TXTINP('Input REPORT-DATE as MBGL-20140101 ', 8, 25,15, 'ANY')
    LINE.BAL.ID = COMI

    IF COMI = '' OR COUNT(COMI,'-') =0 THEN
        PRINT "Invalid option or data. Enter the option again "
        SLEEP(3)
        GOTO EXTRACT.LINE.BAL
    END

    REPORT.NAME = FIELD(COMI,'-',1)
    COMI = REPORT.NAME
    GOSUB RE.STAT.HEAD.VALIDATION
    COMI = LINE.BAL.ID
    REPORT.DATE = FIELD(COMI,'-',2)
    COMI = REPORT.DATE
    CALL IN2D(N1,'D')
    IF ETEXT THEN
        CRT @(-1)
        E = ETEXT
        CALL ERR
* PRINT @(5,6) : " INVALID DATE VALUE GIVEN "
* PRINT @(5,8) : " Enter the date in YYYYMMDD format "
        SLEEP(3)
        GOTO EXTRACT.LINE.BAL;
    END

    WRITE.XL = 'ID|OPEN.BAL|OPEN.BAL.LCL|CR.MOVEMENT|CR.MVMT.LCL|DB.MOVEMENT|DB.MVMT.LCL|CLOSING.BAL|CLOSING.BAL.LCL'

    SEL.CMD = 'SELECT ':FN.RE.STAT.LINE.BAL:" LIKE ":REPORT.NAME:"...":REPORT.DATE:"..."
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.RECS,SEL.ERR)
    LOOP
        REMOVE LINE.BAL.ID FROM SEL.LIST SETTING POS
    WHILE LINE.BAL.ID:POS
        READ R.RE.STAT.LINE.BAL FROM F.RE.STAT.LINE.BAL, LINE.BAL.ID THEN
            WRITE.XL<-1>=LINE.BAL.ID:'|':R.RE.STAT.LINE.BAL<RE.SLB.OPEN.BAL>:'|':R.RE.STAT.LINE.BAL<RE.SLB.OPEN.BAL.LCL>:'|':R.RE.STAT.LINE.BAL<RE.SLB.CR.MOVEMENT>:'|':R.RE.STAT.LINE.BAL<RE.SLB.CR.MVMT.LCL>:'|':R.RE.STAT.LINE.BAL<RE.SLB.DB.MOVEMENT>:'|':R.RE.STAT.LINE.BAL<RE.SLB.DB.MVMT.LCL>:'|':R.RE.STAT.LINE.BAL<RE.SLB.CLOSING.BAL>:'|':R.RE.STAT.LINE.BAL<RE.SLB.CLOSING.BAL.LCL>
        END
    REPEAT


    RETURN

*---------------------------------------------------------------------------------------------------

END














