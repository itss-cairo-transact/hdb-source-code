PROGRAM POPULATE.TEST.FILE

$INSERT I_COMMON
$INSERT I_EQUATE

OPEN "F.TEST.FILE" TO F.TEST.FILE ELSE CRT "FILE OPEN ERROR!!!"

EXECUTE "CLEAR.FILE F.TEST.FILE"

FOR I=1 TO 10000
	R.REC=0
	WRITE R.REC ON F.TEST.FILE,I
NEXT I

CRT "Program completed"

STOP
END