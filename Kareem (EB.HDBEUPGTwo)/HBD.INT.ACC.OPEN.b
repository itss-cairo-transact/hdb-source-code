*-----------------------------------------------------------------------------
* <Rating>350</Rating>
*-----------------------------------------------------------------------------
* Version 21 15/05/01  GLOBUS Release No. G15.0.04 29/11/04
    SUBROUTINE HBD.INT.ACC.OPEN (PACCOUNT.NO, PRETURN.CODE)
*
* THIS PROGRAM WILL TAKE AS INPUT AN INTERNAL ACCOUNT NUMBER
* AND, IF POSSIBLE, OPEN A NEW INTERNAL ACCOUNT
* THERE IS ONLY 1 PARAMETER INPUT, THE ACCOUNT NUMBER, AND 1 PARAMETER
* OUTPUT, THE RETURN CODE.
* THE RETURN CODE WILL BE SET AS FOLLOWS:-
*
*          0 - ALL OK. RECORD HAS BEEN CREATED
*
*          5 - ACCOUNT NUMBER INPUT IS INCORRECT
*          6 - ACCOUNT NUMBER EXISTS ON UNAUTHORISED FILE
*          7 - NO SIMILAR ACCOUNT EXISTS ON LIVE FILE
*          8 - ACCOUNT EXISTS ON LIVE FILE
*
* 19/02/97 - GB9700185
*            Set the field OPEN.CATEGORY in the new account.
*
* 13/03/98 - GB9800253
*            Copy STOCK.CONTROL.TYPE to new account.
*            If same currency then copy SERIAL.NO.FORMAT across as well.
* 29.04.98 - GB9800002
*            Default the Account officer from R.USER if we are not
*            Running in the Batch
*
* 08/04/02 - CI_10001128
*            Default INTEREST,CHARGE Currency & ccy mkt when Account is
*            automatically opened.
* 26/11/02 - EN_10001345
*          - When internal accounts are created, STMT2.FQU file
*          - needs to be updated with default date for STMT.FREQU.1
* 19/12/02 - BG_100002917
*          - Shortened few field names.
*
* 06/10/03 - BG_100005305
*            default sub division code for multi book
*
* 15/12/03 - BG_100005847
*            If we are creating an internal account in the master company of
*            a multi book account then check to see if there is an account in the old format
*            If there is then set the old account for closure to settel to the new account
*            with autopay from the old to the new.
*
* 05/01/04 - GLOBUS_BG_100005890
*            Account Statement Processing - Changes done to Get rid of
*            the statement concat file and the wrk file
*
* 31/03/04 - CI_10018613
*            Default AC.OPENING.DATE when account is automatically
*            opened.
*
* 22/07/04 - CI_10021477
*            Setting value for the CONTINGENT.INT field for the
*            Internal Contingent Accounts when account opened automatically
*
* 25/08/04 - CI_10022543
*            The account which has to be created new, is read with Lock
*            to avoid time out error message "unique constraint violation"
* 11/09/04 - CI_10022839
*          - Account read with the wrong record variable(YREC, instead of
*          - YNEW.REC).
*
* 05/04/04 - CI_10028921
*            find lead company using financial.com not default.finan
*
* 27/05/05 - GLOBUS_BG_100008824
*            COMI gets overwritten by use of CFQ routine.
*
* 15/06/04 - CI_10031061
*          - Unable to default suspense account in LD from BOOK company.
*          - System throws error - "USD1450400010003 CANNOT CREATE DEFAULT".
*
* 13/10/05 - CI_10035490
*            Make sure the corrrect company is loaded if an MB style internal account is being
*            opened so that the CO.CODE is set correctly. Return an error if an invalid sub division
*            code is present, make sure we have the lock if creating an account
*
* 19/09/06 - CI_10044135
*          - System fails to update ACCOUNT.TITLE.1,2 & SHORT.TITLE when called from
*          - browser, coz GLOBUS.INITIALISE will clear T.REMTEXT. Hence harding-coding
*          - T.REMTEXT(58)
*
* 29/08/07 - EN_10003461 / BG_100015411
*            Amend to cope with Position Accounts
*
* 07/11/07 - BG_100015741
*            Position accounts created with formated account officer code
*
* 18/12/07 - BG_100016384
*            Set DEALER.DEAK.ID from the account number passed.
*
* 07/03/08 - CI_10054022
*            Variable uninitialised error during cob.
*
* 03/07/08 - CI_10056456
*            Account Officer is passed to INT.ACC.OPEN. For Position
*            Accounts, use this account officer is the dealer desks
*            don't match.
*
* 27/04/09 - CI_10062432
*            ACCOUNT.CLOSED record for internal account is deleted when
*            an internal account is re-opened.
*
* 25/06/09 - CI_10064000
*            Only update CATEG.INT.ACCT for the first account and teller
*            cash accounts.
*
* 06/10/09 - EN_10004351
*            Archive ENTRY.DETAIL and suppress statements.
*
* 17/02/11 - Patch R071027  no longer working-Reference internal a/c wrongly fetched in
*            automatic till a/c opening process
*            Defect : 73021/ Coding Task : 155785
*
* 29/03/11 - Defect 180191
*            variable YR.CATEG.INT.ACCT not initialised.
*
* 07/07/2011 - ENHANCEMENT 211020 / TASK 211051
*              High Volume Account Layout changes and Merge service
*
* 04/01/2012 - DEFECT 321315 / TASK 333965
*              Load the account company for HVT accounts inoreder to generate CONSOL.KEY
*              in account company.
*
* 13/01/12 - DEFECT 154185 / TASK 157385
*            To update NEXT.ACCT.STMT file, if it's there in the current installation.
*
* 21/02/12 - Task 359661
*              Default HVT FLAG for accounts based on set up done in AC.AUTO.ACCOUNT.
*
* 27/02/12 -  Task 362268
*            Changes done to avoid Defaulting HVT flag for Foreign ccy Accounts.
*
* 29/02/12 - Task 363812
*            Since the EN-154815 is exculded in Regression Run,the call to the
*            subroutine "AC.UPDATE.NEXT.STMT" which is introduced through the Enhancement is removed
*            to avoid 'subroutine call failed' error.
*
* 15/03/12 - Task 372712
*            The code changes done through the above task is reversed as the EN-154185 is
*            included in Regression Run.
*
* 26/10/12 - Enhancement - 398148 / Task - 450910
*            Multiple Posting Restrictions at Customer and Account level.
*
* 21/12/12  -  Task 547257
*              Account parameter insert is included to avoid compilation warning.
*
* 17/07/13 - Defect - 722825/ Task - 731442
*            Assign latest date and time in TIME.STAMP variable.
*
* 20/08/13  - Defect 761447 / Task 761450
*             Performance fix to update HVT flag as YES for all internal accounts when SEAT is ON
*
*
* 09/04/13 - ENHANCEMENT 825138 / TASK 967793
*            The new API AC.DETERMINE.HVT.FLAG is called to assign the HVT.FLAG.
*
* 08/10/2014 - DEFECT 1272152 / TASK 1368247
*              For HVT accounts open asset type is updated on authorisation of account
*              based on Account parameter, conitngent and non contingent type.
*
************************************************************************************
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.COMPANY
    $INSERT I_F.ACCOUNT
    $INSERT I_F.ACCOUNT.STATEMENT
    $INSERT I_F.USER
    $INSERT I_F.ACCOUNT.CLOSURE
    $INSERT I_F.DEALER.DESK
    $INSERT I_F.COMPANY.CHECK
    $INSERT I_F.TELLER.PARAMETER
    $INSERT I_F.EB.CONTRACT.BALANCES
    $INSERT I_F.AC.AUTO.ACCOUNT
    $INSERT I_F.ACCOUNT.PARAMETER
    $INSERT I_F.SPF
*
    COM /TT.TRAN.CATEGORIES/ TT.TRAN.CATEGORIES
*
    GOSUB INITIALISE
    IF NOT(PRETURN.CODE) THEN
        GOSUB OPEN.ACCOUNT
    END
    IF SAVE.COMPANY NE ID.COMPANY THEN
        CALL LOAD.COMPANY(SAVE.COMPANY)
    END

    RETURN
***************************************************************************
INITIALISE:
***********

    TT.MATCH.ACC = ""
    YNEW.REC = ""
    YSAME.CCY = ""
    SAVE.COMI = COMI          ;* BG_100008824
    PRETURN.CODE = 0
    SAVE.COMPANY = ID.COMPANY
    YR.CATEG.INT.ACCT = ""

    YACC.OFF = PACCOUNT.NO<2>
    PACCOUNT.NO = PACCOUNT.NO<1>
    TIME.STAMP = TIMEDATE()

    IF NUM(PACCOUNT.NO[4,3]) THEN
        *This is not a position account.
        YPOSITION.AC = 0
    END ELSE
        *This is a position account, use this variable to add 3 to length checks or
        *to act as a true value.
        YPOSITION.AC = 3
    END
*
**************CHECK LENGTH OF INPUT ACCOUNT NUMBER**************
*
    IF LEN(PACCOUNT.NO) < (12 + YPOSITION.AC) THEN
        PRETURN.CODE = 5
        RETURN
    END

    GOSUB CHECK.COMPANY
    IF PRETURN.CODE THEN
        RETURN
    END
*
**************INITIALISE WORK FIELDS AND OPEN FILES**************
*
    PRETURN.CODE = 0
    F.ACCOUNT$NAU = ""
    YFULL.FNAME = "F.ACCOUNT"
    YNAU.FNAME = "F.ACCOUNT$NAU"
    YID.NEW = PACCOUNT.NO
    ID.CONCATFILE = "AR"
    CALL OPF("F.ACCOUNT$NAU",F.ACCOUNT$NAU)
    F.ACCOUNT = ""
    CALL OPF(YFULL.FNAME,F.ACCOUNT)
*
    YMATCHED.KEY = '' ; YBOOK = ''
    IF ID.COMPANY <> R.COMPANY(EB.COM.FINANCIAL.COM) THEN
        YBOOK = 1
    END
*
*  Setup the character positions to extract information from account keys.
*
    YPOS.CATEG   = 4 + YPOSITION.AC
    YPOS.DEPT.CD = 9 + YPOSITION.AC
    YPOS.SUB.DIV = 13 + YPOSITION.AC
*
    PACCOUNT.CATEG  = PACCOUNT.NO[YPOS.CATEG,5]
    PACCOUNT.DEPT.CD = PACCOUNT.NO[YPOS.DEPT.CD,4]
    PACCOUNT.SUB.DIV = PACCOUNT.NO[YPOS.SUB.DIV,4]
*
**************READ AUTHORISED FILE**************
*
    ETEXT = ""
    YREC = ""
    CALL F.READ(YFULL.FNAME,PACCOUNT.NO,YREC,F.ACCOUNT,ETEXT)
    IF YREC <> "" THEN
        PRETURN.CODE = 8
        RETURN
    END
*
**************READ UNAUTHORISED FILE**************
*
    YKEY = PACCOUNT.NO
    ETEXT = ""
    YREC = ""
    CALL F.READ(YNAU.FNAME,YKEY,YREC,F.ACCOUNT$NAU,ETEXT)
    IF YREC <> "" THEN
        PRETURN.CODE = 6
        RETURN
    END
    TT.CATEG = @FALSE
    LOCATE "TT" IN R.COMPANY(EB.COM.APPLICATIONS)<1, 1> SETTING TT.INSTALLED THEN
        IF UNASSIGNED(TT.TRAN.CATEGORIES) OR NOT(TT.TRAN.CATEGORIES) THEN       ;* Load teller categories
            GOSUB LOAD.TT.PARAMETER
        END
        LOCATE PACCOUNT.CATEG IN TT.TRAN.CATEGORIES<1> SETTING P.ACCOUNT.CATEG.POS THEN
            TT.CATEG = @TRUE
        END
    END

    GOSUB CHECK.CATEG.INT.ACCT

    RETURN

*-----------------------------------------------------------------------------

*** <region name= LOAD.TT.PARAMETER>
LOAD.TT.PARAMETER:
*** <desc>Load teller parameter records </desc>

    TT.TRAN.CATEGORIES = ""
    ER = ""
    CALL CACHE.READ("F.COMPANY.CHECK", "FIN.FILE", R.COMPANY.CHECK, ER)
    ID.TELLER.PARAMETER.LIST = R.COMPANY.CHECK<EB.COC.COMPANY.CODE>

    LOOP
        REMOVE ID.TELLER.PARAMETER FROM ID.TELLER.PARAMETER.LIST SETTING ID.TELLER.PARAMETER.MARK
    WHILE ID.TELLER.PARAMETER : ID.TELLER.PARAMETER.MARK
        ER = ""
        CALL F.READ("F.TELLER.PARAMETER", ID.TELLER.PARAMETER, R.TELLER.PARAMETER, "", ER)
        IF NOT(ER) THEN
            TT.CATEGS = R.TELLER.PARAMETER<TT.PAR.TRAN.CATEGORY>
            LOOP REMOVE TT.CAT FROM TT.CATEGS SETTING TTP
            WHILE TT.CAT:TTP
                LOCATE TT.CAT IN TT.TRAN.CATEGORIES<1> SETTING TT.CAT.POS ELSE
                    TT.TRAN.CATEGORIES<TT.CAT.POS> = TT.CAT
                END
            REPEAT
        END
    REPEAT


    RETURN

*** </region>

******************************************************************************
CHECK.COMPANY:
**************
    COMP.CODE = ID.COMPANY
    IF LEN(PACCOUNT.NO) = (16 + YPOSITION.AC) THEN          ;* Set up the company code for the branch
        GOSUB CHECK.BRANCH
    END ELSE        ;* we would just default this company anyway
        IF C$MULTI.BOOK AND LEN(PACCOUNT.NO) = (12 + YPOSITION.AC) THEN
            PACCOUNT.NO := R.COMPANY(EB.COM.SUB.DIVISION.CODE)
        END
    END

    RETURN

******************************************************************************
CHECK.BRANCH:
*************
    SUBDIV = PACCOUNT.NO[4]
    CALL CACHE.READ("F.COMPANY.CHECK","SUB.DIVISION",R.COMP.CHECK,ER)
    LOCATE SUBDIV IN R.COMP.CHECK<1,1> SETTING POS THEN
        COMP.CODE = R.COMP.CHECK<3,POS,1>         ;* Company of subdivision code
        CALL CACHE.READ("F.COMPANY",COMP.CODE,R.COMP,ER)
        IF R.COMP<EB.COM.BOOK> = "" THEN          ;* Its a standalone company
            PRETURN.CODE = 5
        END ELSE
            IF R.COMP<EB.COM.FINANCIAL.COM> NE R.COMPANY(EB.COM.FINANCIAL.COM) THEN       ;* Different categ int acct file
                CALL LOAD.COMPANY(COMP.CODE)
            END
        END
    END ELSE        ;* invalid sub division code
        PRETURN.CODE = 5
    END
    RETURN

************************************************************************
CHECK.CATEG.INT.ACCT:
*********************
*************************************************************************
****  Read 'F.CATEG.INT.ACCT' to obtain table of internal account    ****
****  number with same category.                                     ****
*************************************************************************
*

    LOCATE "TT" IN R.COMPANY(EB.COM.APPLICATIONS)<1, 1> SETTING TT.INSTALLED THEN
        IF APPLICATION = "TELLER" THEN
            TT.MATCH.ACC = PACCOUNT.NO
            CALL TT.GET.MATCH.INT.ACC(TT.MATCH.ACC)
            IF TT.MATCH.ACC<2> NE '' THEN   ;*R.CATEG.INT.ACCT record not found
                ETEXT = "RECORD NOT FOUND"
                PRETURN.CODE = 7
                RETURN
            END
            YKEY = TT.MATCH.ACC
            * Since SET.CURRENCY.FLAG, will clear the value of YKEY if not same CCY, so we need to assign it again
            GOSUB SET.CURRENCY.FLAG
            YKEY = TT.MATCH.ACC
            RETURN
        END
    END
    YFILE.NAME = "F.CATEG.INT.ACCT"
    YF.CATEG.INT.ACCT = ""
    CALL OPF(YFILE.NAME,YF.CATEG.INT.ACCT)
*
    CALL F.READ(YFILE.NAME,PACCOUNT.CATEG,YR.CATEG.INT.ACCT,YF.CATEG.INT.ACCT,ETEXT)
    IF ETEXT OR YR.CATEG.INT.ACCT = "" THEN
        ETEXT = ""
        * NO MATCHING ACCOUNTS FOUND
        PRETURN.CODE = 7
        RETURN
    END
*
* Ensure that this CATEG.INT.ACCT category only contains matching
* Position Accounts or Non Position Accounts. Can't mix.
*
    IF NUM(YR.CATEG.INT.ACCT<1>[4,3]) THEN
        CATEG.POSITION.AC = 0
    END ELSE
        CATEG.POSITION.AC = 3
    END

    IF CATEG.POSITION.AC <> YPOSITION.AC THEN
        * There is a mismatch between the category account type and this
        * account.
        PRETURN.CODE = 7
        RETURN
    END
*
    YREC = ""
*
**************FIND ACCOUNT WITH SAME CURRENCY AND CATEGORY**************
*
    YCHECK.LEN = 8 + YPOSITION.AC
    YFIND = PACCOUNT.NO[1,YCHECK.LEN]:"9999"
* Find the position of the highest id, CCYCCCCC9999 or CCYCCYCCCCC9999
    LOCATE YFIND IN YR.CATEG.INT.ACCT<1> BY "AR" SETTING YLOC ELSE
        NULL
    END
    IF YLOC > 1 THEN
        YLOC -= 1
    END

    YKEY = YR.CATEG.INT.ACCT<YLOC>
* Check to see if the LOCATE is positioned at a block of the same CCYCCCCC
    GOSUB SET.CURRENCY.FLAG
*
**************FIND ACCOUNT WITH SAME CATEGORY AND SUB.DIV**************
*
    IF YKEY = "" THEN
        YAF = 1
        YFIND = PACCOUNT.NO[YPOS.CATEG,9]
        LOOP
            YKEY = YR.CATEG.INT.ACCT<YAF>
        UNTIL YKEY = "" DO
            GOSUB FIND.ACCOUNT
            IF ACCT.FOUND THEN          ;* we found a match
                EXIT
            END
            YAF += 1
        REPEAT
    END

    IF YMATCHED.KEY THEN
        YKEY = YMATCHED.KEY
    END
*
*************************************************************************
****  Use first account number with same category                    ****
*************************************************************************
*
    IF YKEY = "" THEN
        YKEY = YR.CATEG.INT.ACCT<1>
    END
    RETURN

***************************************************************************
SET.CURRENCY.FLAG:
*****************
    IF PACCOUNT.NO[1,8] = YKEY[1,8] THEN
        * Donot set YSAME.CCY,  IF belongs to the book comp.
        IF YBOOK AND PACCOUNT.SUB.DIV <> YKEY[13,4] THEN
            YSAME.CCY = ''
        END ELSE
            YSAME.CCY = 'Y'
        END
    END ELSE
        YKEY = ''
    END
    RETURN
******************************************************************************
FIND.ACCOUNT:
*************
    ACCT.FOUND = ""
* Find account with same CATEGORY and SUB-DIV then with DEPT.CODE for BOOK comp.
    IF YBOOK THEN   ;* Check for Book comp.
        IF YKEY[YPOS.CATEG,5]:YKEY[YPOS.SUB.DIV,4] = PACCOUNT.CATEG:PACCOUNT.SUB.DIV THEN
            YMATCHED.KEY = YKEY
            IF YKEY[YPOS.DEPT.CD,4] = PACCOUNT.DEPT.CD THEN
                ACCT.FOUND = 1
            END
        END
    END ELSE
        * This is for LEAD & other MC.
        IF YKEY[YPOS.CATEG,9] = YFIND THEN
            ACCT.FOUND = 1
        END
    END
    RETURN
***************************************************************************
OPEN.ACCOUNT:
*************
*
*************************************************************************
****  Create new account                                             ****
*************************************************************************
*
    FN.ACCOUNT.CLOSED = 'F.ACCOUNT.CLOSED'
    F.ACCOUNT.CLOSED = ''
    CALL OPF(FN.ACCOUNT.CLOSED,F.ACCOUNT.CLOSED)
    YERR = ''
    R.ACCOUNT.CLOSED = ''
    CALL F.READ(FN.ACCOUNT.CLOSED,PACCOUNT.NO,R.ACCOUNT.CLOSED,F.ACCOUNT.CLOSED,YERR)
    IF R.ACCOUNT.CLOSED THEN
        CALL F.DELETE(FN.ACCOUNT.CLOSED,PACCOUNT.NO)
    END
    ETEXT = ""
    CALL F.READ(YFULL.FNAME,YKEY,YREC,F.ACCOUNT,ETEXT)      ;* read the base record
    IF ETEXT THEN
        ETEXT = ""
        PRETURN.CODE = 7
        RETURN
    END

    RETRY = "E"     ;* if its locked by someone else then return imediatly
    CALL F.READU(YFULL.FNAME,PACCOUNT.NO,YNEW.REC,F.ACCOUNT,ER,RETRY)
    IF ER NE "RECORD NOT FOUND" THEN    ;* it already exists or someone else  has a lock on it
        RETURN
    END

* the record does not exist and we have the lock

    AVI.MSG = T.REMTEXT(58)   ;* RECORD.AUTOMATICALLY.OPENED
    IF AVI.MSG = '' THEN
        AVI.MSG = "RECORD.AUTOMATICALLY.OPENED"
        CALL TXT(AVI.MSG)
    END
    FOR LOOP.1 = AC.CUSTOMER TO AC.AUDIT.DATE.TIME
        GOSUB BUILD.ACCOUNT.RECORD
    NEXT LOOP.1
    CALL F.WRITE("F.ACCOUNT",PACCOUNT.NO,YNEW.REC)

    GOSUB UPDATE.ECB.FOR.HVT.ACCOUNTS ;* Call after Write is done to the opened account, so that CONSOL.KEY will be generated.

* If multi book then check to see if there is an account in the old format i.e. 12 character key
* if it is for this (master) company then mark it for closure, set autopay and write closure record.

    IF C$MULTI.BOOK THEN
        GOSUB CHECK.CLOSURE
    END

    IF  YR.CATEG.INT.ACCT EQ "" OR TT.CATEG THEN
        YSAVE = ID.NEW
        ID.NEW = PACCOUNT.NO
        CALL TABLE.FILE.UPDATE("AR":FM:"CATEG.INT.ACCT","",PACCOUNT.CATEG)
        ID.NEW = YSAVE
    END
*
* Raise a default account.statement record for the account being
* created

    RETURN

**********************************************************************************************

BUILD.ACCOUNT.RECORD:
*********************

    BEGIN CASE
        CASE LOOP.1 = AC.OPEN.CATEGORY
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.CATEGORY
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.ACCOUNT.TITLE.1
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,AVI.MSG)
        CASE LOOP.1 = AC.ACCOUNT.TITLE.2
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,AVI.MSG)
        CASE LOOP.1 = AC.SHORT.TITLE
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,AVI.MSG)
        CASE LOOP.1 = AC.POSITION.TYPE
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.CURRENCY
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,PACCOUNT.NO[1,3])
        CASE LOOP.1 = AC.CURRENCY.MARKET
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.CHARGE.CCY         ;* CI_10001128 S
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YNEW.REC<AC.CURRENCY>)
        CASE LOOP.1 = AC.INTEREST.CCY
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YNEW.REC<AC.CURRENCY>)
        CASE LOOP.1 = AC.CHARGE.MKT
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.INTEREST.MKT
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.OPENING.DATE       ;* CI_10018613 S
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,TODAY)        ;* CI_10018613 E
        CASE LOOP.1 = AC.ACCOUNT.OFFICER
            IF YPOSITION.AC THEN
                IF YKEY[12,2] = PACCOUNT.NO[12,2] THEN
                    DEPT.ACCT.OFFICER = YREC<AC.ACCOUNT.OFFICER>
                END ELSE
                    BEGIN CASE
                        CASE YACC.OFF # ''
                            DEPT.ACCT.OFFICER = YACC.OFF
                        CASE 1
                            DEPT.ACCT.OFFICER = YREC<AC.ACCOUNT.OFFICER>
                    END CASE
                END
                YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,DEPT.ACCT.OFFICER)
            END ELSE
                IF RUNNING.UNDER.BATCH THEN
                    YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
                END ELSE
                    YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,R.USER<EB.USE.DEPARTMENT.CODE>)
                END
            END
        CASE LOOP.1 = AC.OTHER.OFFICER
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
            * GB9800253
        CASE LOOP.1 = AC.STOCK.CONTROL.TYPE
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.SERIAL.NO.FORMAT
            IF YSAME.CCY = 'Y' THEN
                YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
            END
            * CI_10021477 starts
        CASE LOOP.1 = AC.CONTINGENT.INT
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
            * CI_10021477 ends
        CASE LOOP.1 = AC.LOCAL.REF
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.CURR.NO
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,"1")
        CASE LOOP.1 = AC.INPUTTER
            YINP = TNO:"_":OPERATOR
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YINP)
        CASE LOOP.1 = AC.DATE.TIME
            YTIME = OCONV(DATE(),"D-")
            YTIME = YTIME[9,2]:YTIME[1,2]:YTIME[4,2]:TIME.STAMP[1,2]:TIME.STAMP[4,2]
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YTIME)
        CASE LOOP.1 = AC.AUTHORISER
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,"SY_":YKEY)
        CASE LOOP.1 = AC.CO.CODE
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,COMP.CODE)
        CASE LOOP.1 = AC.DEPT.CODE
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,YREC<LOOP.1>)
        CASE LOOP.1 = AC.HVT.FLAG
            GOSUB DEFAULT.HVT.FLAG ; *Determines whether HVT flag need to be set.
        CASE OTHERWISE
            YNEW.REC = INSERT(YNEW.REC,LOOP.1,0,0,"")
    END CASE
    RETURN

*-----------------------------------------------------------------------------
*** <region name= DEFAULT.HVT.FLAG>
DEFAULT.HVT.FLAG:
*-------------------
*** <desc>Checks the account and determines whether the account can be marked as "HVT" or not. </desc>
*Determines whether HVT flag need to be set
* If the new parameter is not setup then there should not be any change in updation of account HVT flag
* so UPDATE.FLAG.IN.ACC will be set if the parameter is not created, in such case update the HVT flag in
* account and ECB as usual

    ACCOUNT.HVT.FLAG = ''
    UPDATE.FLAG.IN.ACC = ''
    CALL AC.DETERMINE.HVT.FLAG(PACCOUNT.NO, YNEW.REC, '', '', ACCOUNT.HVT.FLAG, UPDATE.FLAG.IN.ACC, '') ;*returns the HVT.FLAG must be set to YES/NO for the passed account record
    IF UPDATE.FLAG.IN.ACC THEN ;* Parameter not created, Update the HVT flag in account
        YNEW.REC<AC.HVT.FLAG> = ACCOUNT.HVT.FLAG
    END

    RETURN
*** </region>
*-----------------------------------------------------------------------------
*** <region name= UPDATE.ECB.FOR.HVT.ACCOUNTS>
UPDATE.ECB.FOR.HVT.ACCOUNTS:
*** <desc>Update EB.CONTRACT.BALANCES for HVT accounts </desc>

    IF ACCOUNT.HVT.FLAG NE 'YES' THEN
        RETURN
    END

    ACCOUNT.COMPANY = ''

    ACCOUNT.COMPANY = YNEW.REC<AC.CO.CODE>
    SAVE.OLD.COMPANY = ID.COMPANY

    IF ACCOUNT.COMPANY NE ID.COMPANY THEN
        CALL LOAD.COMPANY(ACCOUNT.COMPANY)
    END

* Generate consol key if HVT.FLAG is set and write them into ECB.
    FN.EB.CONTRACT.BALANCES = 'F.EB.CONTRACT.BALANCES'
    F.EB.CONTRACT.BALANCES = ''
    CALL OPF(FN.EB.CONTRACT.BALANCES,F.EB.CONTRACT.BALANCES)

    R.CONTRACT.BALANCES = ''
    YERR = ''
    CALL F.READU(FN.EB.CONTRACT.BALANCES,PACCOUNT.NO, R.CONTRACT.BALANCES,F.EB.CONTRACT.BALANCES,YERR,'')

    IF YERR THEN
        * It is first time update to Master Record so populate these details
        ERROR.MESS = ''
        ENTRY.REC = ''
        CONSOL.KEY = ''
        APP.ID = "AC"
        CALL RE.GEN.AL.KEY(APP.ID,'',R.CONTRACT.BALANCES,PACCOUNT.NO,CONSOL.KEY,ERROR.MESS)
        R.CONTRACT.BALANCES<ECB.CONSOL.KEY> = CONSOL.KEY
        GOSUB POPULATE.OPEN.ASSET.TYPE ;* Determine the initial asset type.
        R.CONTRACT.BALANCES<ECB.PRODUCT> = "AC"
        R.CONTRACT.BALANCES<ECB.APPLICATION> = "ACCOUNT"
        R.CONTRACT.BALANCES<ECB.CO.CODE> = YNEW.REC<AC.CO.CODE>
        R.CONTRACT.BALANCES<ECB.CURRENCY> = YNEW.REC<AC.CURRENCY>
        R.CONTRACT.BALANCES<ECB.BALANCE.MOVED> = 'YES'
        *
        YPARAMS = R.CONTRACT.BALANCES<ECB.CONSOL.KEY>:FM:PACCOUNT.NO
        CALL RE.UPDATE.LINK.FILE(YPARAMS,"") ;* Update the link file for the HVT account
    END

    R.CONTRACT.BALANCES<ECB.HVT.FLAG> = YNEW.REC<AC.HVT.FLAG>
    R.CONTRACT.BALANCES<ECB.DATE.LAST.UPDATE> = TODAY
    CALL EB.WRITE.CONTRACT.BALANCES(PACCOUNT.NO, R.CONTRACT.BALANCES)

    IF SAVE.OLD.COMPANY NE ID.COMPANY THEN
        CALL LOAD.COMPANY(SAVE.OLD.COMPANY)
    END

    RETURN
*** </region>

POPULATE.OPEN.ASSET.TYPE:
***********************

* call the routine AC.DETERMINE.INIT.ASSET.TYPE to determine the correct initial asset type based on the setup in
* ACCOUNT.PARAMETER before writing the record into ECB.
    GOSUB DETERMINE.ASSET.TYPE
    R.CONTRACT.BALANCES<ECB.OPEN.ASSET.TYPE> = ASSET.TYPE

    RETURN
*-----------------------------------------------------------------------------------------------------------
DETERMINE.ASSET.TYPE:
*********************

    ASSET.TYPE = 'NEW'
    MVMT.AMT = 1 ;* Set to get the Credit balance by default
    R.ACC = YNEW.REC
*ASSET.TYPE is determined by calling AC.DETERMINE.INIT.ASSET.TYPE
    CALL AC.DETERMINE.INIT.ASSET.TYPE(PACCOUNT.NO,R.ACC,ASSET.TYPE, MVMT.AMT)
    RETURN

****************************************************************************************
CHECK.CLOSURE:
**************

    IF YNEW.REC<AC.CO.CODE> = R.COMPANY(EB.COM.FINANCIAL.COM) THEN    ;* Lead company

        OLD.ACCOUNT.ID = PACCOUNT.NO[1,12 + YPOSITION.AC]
        ER = ""
        CALL F.READ(YFULL.FNAME,OLD.ACCOUNT.ID,R.ACCOUNT,F.ACCOUNT,ER)

        IF NOT(ER) THEN       ;* account in old format found
            GOSUB CLOSE.ACCOUNT
        END
    END
    RETURN

****************************************************************************************
CLOSE.ACCOUNT:
**************

    CLOSURE.FLAG = 0 ; RETURN.ERROR = ''
    POST.RESTRICT = R.ACCOUNT<AC.POSTING.RESTRICT>          ;* New Posting Restrict Values
    CALL AC.GET.POSTING.RESTRICTION.DETAILS('ACCOUNT', '', POST.RESTRICT, CLOSURE.FLAG, RETURN.ERROR)

    IF R.ACCOUNT<AC.CO.CODE> = YNEW.REC<AC.CO.CODE> AND R.ACCOUNT<AC.AUTO.PAY.ACCT> = "" AND NOT(CLOSURE.FLAG) THEN

        * Old internal account found that is not set for closure

        FN.ACCOUNT.CLOSURE = "F.ACCOUNT.CLOSURE$NAU" ; F.ACCOUNT.CLOSURE = "" ; ER = ""
        CALL OPF(FN.ACCOUNT.CLOSURE,F.ACCOUNT.CLOSURE)
        CALL F.READ(FN.ACCOUNT.CLOSURE,OLD.ACCOUNT.ID,R.ACCOUNT.CLOSURE,F.ACCOUNT.CLOSURE,ER)

        IF ER THEN  ;* no unauth record

            FN.ACCOUNT.CLOSURE = "F.ACCOUNT.CLOSURE" ; F.ACCOUNT.CLOSURE = "" ; ER = ""
            CALL OPF(FN.ACCOUNT.CLOSURE,F.ACCOUNT.CLOSURE)
            CALL F.READ(FN.ACCOUNT.CLOSURE,OLD.ACCOUNT.ID,R.ACCOUNT.CLOSURE,F.ACCOUNT.CLOSURE,ER)

            IF ER THEN        ;* No auth record so good
                CALL F.READU(FN.ACCOUNT.CLOSURE,OLD.ACCOUNT.ID,R.ACCOUNT.CLOSURE,F.ACCOUNT.CLOSURE,ER,"")     ;* Lock it
                DIM R.NEW.SAVE(C$SYSDIM) ; MAT R.NEW.SAVE = MAT R.NEW ; MAT R.NEW = ""
                CALL AC.BUILD.DEFAULT.ACCT.CL(OLD.ACCOUNT.ID,R.ACCOUNT,"")      ;* build default record
                MATBUILD R.ACCOUNT.CLOSURE FROM R.NEW
                MAT R.NEW = MAT R.NEW.SAVE

                R.ACCOUNT.CLOSURE<AC.ACL.POSTING.RESTRICT> = "90"
                R.ACCOUNT.CLOSURE<AC.ACL.SETTLEMENT.ACCT> = PACCOUNT.NO

                R.ACCOUNT.CLOSURE<AC.ACL.INPUTTER> = YNEW.REC<AC.INPUTTER>
                R.ACCOUNT.CLOSURE<AC.ACL.DATE.TIME> = YNEW.REC<AC.DATE.TIME>
                R.ACCOUNT.CLOSURE<AC.ACL.AUTHORISER> = YNEW.REC<AC.AUTHORISER>
                R.ACCOUNT.CLOSURE<AC.ACL.CO.CODE> = YNEW.REC<AC.CO.CODE>
                R.ACCOUNT.CLOSURE<AC.ACL.DEPT.CODE> = YNEW.REC<AC.DEPT.CODE>

                CALL F.WRITE(FN.ACCOUNT.CLOSURE,OLD.ACCOUNT.ID,R.ACCOUNT.CLOSURE)

                * read and lock the old account record for update
                * Set up the old account to autopay to the new, set posting restrict

                CALL F.READU(YFULL.FNAME,OLD.ACCOUNT.ID,R.ACCOUNT,F.ACCOUNT,ER,"")
                IF R.ACCOUNT.PARAMETER<AC.PAR.POSTING.RESTRICT> = 'SINGLE' THEN
                    R.ACCOUNT<AC.POSTING.RESTRICT> = "90"
                END ELSE
                    R.ACCOUNT<AC.POSTING.RESTRICT, -1> = "90"
                END
                R.ACCOUNT<AC.AUTO.PAY.ACCT> = PACCOUNT.NO
                CALL F.WRITE(YFULL.FNAME,OLD.ACCOUNT.ID,R.ACCOUNT)
                CALL F.WRITE("F.AC.AUTO.PAY.TODAY",OLD.ACCOUNT.ID,"") ;* will trigger fwd txn regen
            END
        END
    END

    RETURN
*
**************FATAL.ERROR ROUTINE**************
*
FATAL.ERROR:
    TEXT = "FATAL ERROR: PGM=INT.ACC.OPEN ":TEXT ; CALL REM
    PRETURN.CODE = "TRUE"
    RETURN
*
**************PROGRAM EXIT**************
*
PROG.EXIT:
    RETURN

    END
