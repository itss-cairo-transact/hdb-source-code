    SUBROUTINE FT.CR.ACC3.R.M

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.FUNDS.TRANSFER
    $INSERT I_F.HDB.LAND.ISKAN.M

     GOSUB OPENFILES
	 GOSUB OPENLOG
     GOSUB PROCESS
     GOSUB PROG.END

     RETURN
	
*************************************************************************
OPENFILES:
	
        Y.CD.ID = ""
        RET.CODE = ""
        FN.HDB = "F.HDB.LAND.ISKAN.M"
        F.CD = ""
        F.HDB = ""
        R.CD = ""
        CALL OPF(FN.HDB,F.HDB)

   RETURN
*************************************************************************
OPENLOG:
	F.LOG =''
	Y.PATH.LOG = '/t24/build/T24/LAND/'
	FILE.NAME.LOG = 'LOG':TODAY
	OPENSEQ Y.PATH.LOG,FILE.NAME.LOG TO F.LOG ELSE 
		CREATE F.LOG THEN 	
		END
	END
  RETURN
*************************************************************************

PROCESS: 

    CALL MULTI.GET.LOC.REF("FUNDS.TRANSFER","MBSC.NOTE",CD.TYPE.POS)
        Y.CD.ID = R.NEW(FT.LOCAL.REF)<1,CD.TYPE.POS,2>

        *Y.CD.ID = R.NEW(FT.MBSC.NOTE)<1,2> 
           *Y.CD.ID = "SOC027222473"
        CALL F.READ(FN.HDB,Y.CD.ID,R.CD,F.HDB,RET.CODE)  
	*IF R.CD<LND.ISK.M.LAND.RE.FLAG> = 2 THEN
  	*E = "AA-AC.HDB.LAND.ISKAN.R2"
            *CALL STORE.END.ERROR
             *CALL ERR ; MESSAGE='REPEAT'        
	*END   

	IF RET.CODE EQ '' THEN
        R.CD<LND.ISK.M.LAND.RE.FLAG> = 2 
        R.CD<LND.ISK.M.LAND.ACCTNO>  = R.NEW(FT.CREDIT.ACCT.NO)
        R.CD<LND.ISK.M.LAND.TELR.REF> = ID.NEW
        *R.CD<LND.ISK.M.LAND.DOWN.PAY> = 350000
        CALL F.WRITE(FN.HDB,Y.CD.ID,R.CD)
	 LIGNE='DONE':',':Y.CD.ID:',':R.NEW(FT.DEBIT.ACCT.NO):',': ID.NEW
	END ELSE
		LIGNE='ERROR':',':RET.CODE:',':Y.CD.ID:',':R.NEW(FT.DEBIT.ACCT.NO):',':ID.NEW
		
	END
	WRITESEQ LIGNE APPEND TO F.LOG THEN 
		END 
		CLOSESEQ F.LOG
		LIGNE=''
   RETURN

PROG.END:
        END

RETURN
*********
END

